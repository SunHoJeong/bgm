package com.suno.bgm.ui.search_stat;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.suno.bgm.MainActivity;
import com.suno.bgm.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by suno on 2017. 8. 4..
 */

public class SearchStatFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_search_stat, container, false);
        ButterKnife.bind(this, v);

        ((MainActivity)getActivity()).setToolbarTitle(getResources().getString(R.string.search_stat));

        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @OnClick(R.id.linearLayout_search)
    public void searchBtnClicked(View v){
        Intent intent = new Intent(getActivity(), SearchStatActivity.class);
        startActivity(intent);
    }
}
