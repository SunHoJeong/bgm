package com.suno.bgm.util.db;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.suno.bgm.base.BaseItem;
import com.suno.bgm.model.search_stat.RecentSearch;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by suno on 2017. 8. 8..
 */

public class RecentSearchDao extends DbContentProvider
        implements IRecentSearchSchema, IRecentSearchDao {

    private Cursor cursor;
    private ContentValues contentValues;

    public RecentSearchDao(SQLiteDatabase db) {
        super(db);
    }

    @Override
    protected RecentSearch cursorToEntity(Cursor cursor) {
        RecentSearch recentSearch = new RecentSearch(BaseItem.TYPE_RECENT_SEARCH);

        int searchIdIndext;

        if (cursor != null) {
            if (cursor.getColumnIndex(COLUMN_SAERCH_ID) != -1) {
                searchIdIndext = cursor.getColumnIndexOrThrow(COLUMN_SAERCH_ID);
                recentSearch.setSearchId(cursor.getString(searchIdIndext));
            }
            /*
            if (cursor.getColumnIndex(COLUMN_DATE) != -1) {
               dateIndex = cursor.getColumnIndexOrThrow(COLUMN_DATE);
               user.createdDate = new Date(cursor.getLong(dateIndex));
           }
             */

        }
        return recentSearch;
    }

    @Override
    public RecentSearch fetchDataById(int id) {
        final String selectionArgs[] = { String.valueOf(id) };
        final String selection = id + " = ?";

        RecentSearch recentSearch = new RecentSearch(BaseItem.TYPE_RECENT_SEARCH);
        cursor = super.query(USER_TABLE, USER_COLUMNS, selection,
                selectionArgs, COLUMN_ID);

        if (cursor != null) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                recentSearch = cursorToEntity(cursor);
                cursor.moveToNext();
            }
            cursor.close();
        }

        return recentSearch;
    }

    @Override
    public List<RecentSearch> fetchAllDatas() {
        List<RecentSearch> recentSearchList = new ArrayList<RecentSearch>();
        cursor = super.query(USER_TABLE, USER_COLUMNS, null,
                null, COLUMN_ID);

        if (cursor != null) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                RecentSearch recentSearch = cursorToEntity(cursor);
                recentSearchList.add(recentSearch);
                cursor.moveToNext();
            }
            cursor.close();
        }

        return recentSearchList;
    }

    @Override
    public boolean addData(RecentSearch recentSearch) {
        setContentValue(recentSearch);
        try {
            return super.insert(USER_TABLE, getContentValue()) > 0;
        } catch (SQLiteConstraintException ex){
            Log.w("Database", ex.getMessage());
            return false;
        }
    }

    @Override
    public boolean addDatas(List<RecentSearch> recentSearchList) {
        return false;
    }

    @Override
    public int deleteData(String id) {
        return super.delete(USER_TABLE, COLUMN_SAERCH_ID+"=?", new String[]{id});
    }

    @Override
    public boolean deleteAllDatas() {
        return false;
    }

    private void setContentValue(RecentSearch recentSearch) {
        contentValues = new ContentValues();
        contentValues.put(COLUMN_SAERCH_ID, recentSearch.getSearchId());
    }

    private ContentValues getContentValue(){
        return contentValues;
    }
}
