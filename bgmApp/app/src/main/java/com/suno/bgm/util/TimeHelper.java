package com.suno.bgm.util;

import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by suno on 2017. 8. 14..
 */

public class TimeHelper {
    /* UTC to KST*/
    public static String convertDate(String input){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSS'Z'", Locale.getDefault());
        format.setTimeZone(TimeZone.getTimeZone("UTC"));

        SimpleDateFormat s = new SimpleDateFormat("yyyy년 MM월 dd일 HH시 mm분 ss초", Locale.getDefault());
        s.setTimeZone(TimeZone.getDefault());

        String convertDate = null;
        try {
            convertDate = s.format(format.parse(input));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return convertDate;
    }

    public static String getElapsedDateString(Date date) {

        Date d1 = Calendar.getInstance().getTime();
        Date d2 = date;

        long diff = d1.getTime() - d2.getTime();

        if (diff / (24 * 60 * 60 * 1000) > 0)
            return String.format("%d일 전", diff / (24 * 60 * 60 * 1000));
        if (diff / (60 * 60 * 1000) % 24 > 0)
            return String.format("%d시간 전", diff / (60 * 60 * 1000) % 24);
        if (diff / (60 * 1000) % 60 > 0)
            return String.format("%d분 전", diff / (60 * 1000) % 60);
        if (diff / 1000 % 60 > 0)
            return String.format("%d초 전", diff / 1000 % 60);

        return "";
    }

    public static String calculateTime(Date date) {

        long curTime = System.currentTimeMillis();
        long regTime = date.getTime();
        long diffTime = (curTime - regTime) / 1000;

        Log.d("asdf", curTime + "   " + regTime + "     " + diffTime);


        String msg = null;

        if (diffTime < TIME_MAXIMUM.SEC) {
            // sec
            msg = "방금 전";
        } else if ((diffTime /= TIME_MAXIMUM.SEC) < TIME_MAXIMUM.MIN) {
            // min
            System.out.println(diffTime);

            msg = diffTime + "분 전";
        } else if ((diffTime /= TIME_MAXIMUM.MIN) < TIME_MAXIMUM.HOUR) {
            // hour
            msg = (diffTime) + "시간 전";
        } else if ((diffTime /= TIME_MAXIMUM.HOUR) < TIME_MAXIMUM.DAY) {
            // day
            msg = (diffTime) + "일 전";
        } else if ((diffTime /= TIME_MAXIMUM.DAY) < TIME_MAXIMUM.MONTH) {
            // day
            msg = (diffTime) + "달 전";
        } else {
            msg = (diffTime) + "년 전";
        }

        return msg;
    }

    private static class TIME_MAXIMUM {
        public static final int SEC = 60;
        public static final int MIN = 60;
        public static final int HOUR = 24;
        public static final int DAY = 30;
        public static final int MONTH = 12;
    }
}
