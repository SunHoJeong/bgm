package com.suno.bgm.util;

import com.suno.bgm.model.find_member.FindMemberPosting;

/**
 * Created by suno on 2017. 8. 14..
 */

public class MessageEvent {
    private FindMemberPosting fbPosting;

    public MessageEvent(FindMemberPosting fbPosting) {
        this.fbPosting = fbPosting;
    }

    public FindMemberPosting getFbPosting() {
        return fbPosting;
    }

    public void setFbPosting(FindMemberPosting fbPosting) {
        this.fbPosting = fbPosting;
    }
}
