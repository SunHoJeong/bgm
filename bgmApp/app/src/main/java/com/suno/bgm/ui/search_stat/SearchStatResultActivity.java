package com.suno.bgm.ui.search_stat;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.github.ybq.android.spinkit.SpinKitView;
import com.suno.bgm.R;
import com.suno.bgm.application.BgmApplication;
import com.suno.bgm.model.search_stat.Stat;
import com.suno.bgm.model.search_stat.Stat_;
import com.suno.bgm.model.search_stat.UserStat;
import com.suno.bgm.util.TimeHelper;
import com.suno.bgm.util.network.HttpService;

import org.parceler.Parcels;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.R.attr.format;
import static com.suno.bgm.util.TimeHelper.convertDate;

/**
 * Created by suno on 2017. 8. 7..
 */

public class SearchStatResultActivity extends AppCompatActivity implements Callback<UserStat> {
    @BindView(R.id.textView_search_stat_id)
    TextView tvSearchStatId;
    @BindView(R.id.textView_search_stat_update)
    TextView tvSearchStatUpdated;
    @BindView(R.id.editText_search_stat_result)
    EditText etSearchId;
    @BindView(R.id.toolbar_search_stat_result)
    Toolbar toolbar;
    @BindView(R.id.tabLayout_search_stat)
    TabLayout tabLayout;
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.imageView_search_stat_profile)
    ImageView imgvProfile;
    @BindView(R.id.spinKitView_search_stat)
    SpinKitView spinKitView;
    @BindView(R.id.frameLayout_background)
    FrameLayout frameLayout;

    @BindView(R.id.textView_solo_overview_rank_value)
    TextView tvSoloRankValue;
    @BindView(R.id.textView_solo_overview_rating_value)
    TextView tvSoloRating;
    @BindView(R.id.textView_solo_overview_kd_value)
    TextView tvSoloKd;
    @BindView(R.id.textView_solo_overview_win_rate_value)
    TextView tvSoloWinRate;

    @BindView(R.id.textView_duo_overview_rank_value)
    TextView tvDuoRankValue;
    @BindView(R.id.textView_duo_overview_rating_value)
    TextView tvDuoRating;
    @BindView(R.id.textView_duo_overview_kd_value)
    TextView tvDuoKd;
    @BindView(R.id.textView_duo_overview_win_rate_value)
    TextView tvDuoWinRate;

    @BindView(R.id.textView_squad_overview_rank_value)
    TextView tvSquadRankValue;
    @BindView(R.id.textView_squad_overview_rating_value)
    TextView tvSquadRating;
    @BindView(R.id.textView_squad_overview_kd_value)
    TextView tvSquadKd;
    @BindView(R.id.textView_squad_overview_win_rate_value)
    TextView tvSquadWinRate;

    private SearchStatPagerAdapter pagerAdapter;
    private UserStat userStat;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_stat_result);
        ButterKnife.bind(this);

        //spinKitView.setVisibility(View.INVISIBLE);
        frameLayout.setVisibility(View.INVISIBLE);
        getUserStatFromIntent(getIntent());

        initView();
        initTablayout();

        updateData(userStat);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void initView(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        etSearchId.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                switch (actionId) {
                    case EditorInfo.IME_ACTION_SEARCH:
                        Toast.makeText(getApplicationContext(), etSearchId.getText().toString(), Toast.LENGTH_SHORT).show();
                        String searchId = etSearchId.getText().toString();
                        //TODO: add saveRecentSearch

                        HttpService service = BgmApplication.getRetrofit().create(HttpService.class);
                        Call<UserStat> call = service.getUserStat(searchId);
                        //spinKitView.setVisibility(View.VISIBLE);
                        frameLayout.setVisibility(View.VISIBLE);

                        call.enqueue(SearchStatResultActivity.this);

                        return true;

                    default:
                        Toast.makeText(getApplicationContext(), "기본", Toast.LENGTH_LONG).show();
                        return false;
                }
            }
        });
    }

    public void initTablayout(){
        tabLayout.addTab(tabLayout.newTab().setText("OVERVIEW"));
        tabLayout.addTab(tabLayout.newTab().setText("SOLO"));
        tabLayout.addTab(tabLayout.newTab().setText("DUO"));
        tabLayout.addTab(tabLayout.newTab().setText("SQUAD"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        pagerAdapter = new SearchStatPagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount(), userStat);
        viewPager.setAdapter(pagerAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    public void getUserStatFromIntent(Intent intent){
        if (intent != null) {
            userStat = Parcels.unwrap(intent.getParcelableExtra(UserStat.class.getName()));
        }
    }

    public void updateData(UserStat userStat){
        etSearchId.setText(userStat.getPlayerName());
        etSearchId.setSelection(userStat.getPlayerName().length());

        Glide.with(this).load(userStat.getAvatar()).into(imgvProfile);
        tvSearchStatId.setText(userStat.getPlayerName());
//        if(userStat.getMatchHistory().size() != 0){
//            tvSearchStatRating.setText("Rank:" +userStat.getMatchHistory().get(0).getRatingRank() +"  Rating: "+userStat.getMatchHistory().get(0).getRating());
//
//        }
//        else{
//            tvSearchStatRating.setText("match history is null!");
//        }
        tvSearchStatUpdated.setText(TimeHelper.convertDate(userStat.getLastUpdated()));

        if(userStat.getStats() != null) {
            List<Stat> itemList = userStat.getStats();

            for (Stat stat : itemList) {
                if (stat.getRegion().equals("as") && stat.getSeason().equals("2017-pre3")) {
                    if(stat.getMatch().equals("solo")){
                        List<Stat_> item = stat.getStats();
                        for (Stat_ stat_ : item) {
                            if (stat_.getLabel().equals("Rating")) {
                                tvSoloRating.setText(stat_.getValue());
                                tvSoloRankValue.setText(stat_.getRank() + "");
                            }
                            else if(stat_.getLabel().equals("K/D Ratio")){
                                tvSoloKd.setText(stat_.getValue());
                            }
                            else if(stat_.getLabel().equals("Win %")){
                                tvSoloWinRate.setText(stat_.getValue());
                            }
                        }
                    }
                    else if(stat.getMatch().equals("duo")){
                        List<Stat_> item = stat.getStats();
                        for (Stat_ stat_ : item) {
                            if (stat_.getLabel().equals("Rating")) {
                                tvDuoRating.setText(stat_.getValue());
                                tvDuoRankValue.setText(stat_.getRank() + "");
                            }
                            else if(stat_.getLabel().equals("K/D Ratio")){
                                tvDuoKd.setText(stat_.getValue());
                            }
                            else if(stat_.getLabel().equals("Win %")){
                                tvDuoWinRate.setText(stat_.getValue());
                            }
                        }
                    }
                    else if(stat.getMatch().equals("squad")) {
                        List<Stat_> item = stat.getStats();
                        for (Stat_ stat_ : item) {
                            if (stat_.getLabel().equals("Rating")) {
                                tvSquadRating.setText(stat_.getValue());
                                tvSquadRankValue.setText(stat_.getRank() + "");
                            }
                            else if(stat_.getLabel().equals("K/D Ratio")){
                                tvSquadKd.setText(stat_.getValue());
                            }
                            else if(stat_.getLabel().equals("Win %")){
                                tvSquadWinRate.setText(stat_.getValue());
                            }
                        }
                    }
                }
            }
        }
    }

    public void redirectSearchStatResultActivity(UserStat userStat) {
        Log.d("RETRO", "44");
        Intent intent = new Intent(this, SearchStatResultActivity.class);
        intent.putExtra(UserStat.class.getName(), Parcels.wrap(userStat));
        finish();
        startActivity(intent);
    }

    /* Retrofit Callback */
    @Override
    public void onResponse(Call<UserStat> call, Response<UserStat> response) {
        //spinKitView.setVisibility(View.INVISIBLE);
        frameLayout.setVisibility(View.INVISIBLE);
        etSearchId.setText("");

        if (response.code() == 200) {
            userStat = response.body();

            if (userStat.getAccountId() != null) {
                redirectSearchStatResultActivity(userStat);
            } else {
                Toast.makeText(SearchStatResultActivity.this, "아이디가 존재하지 않습니다!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(SearchStatResultActivity.this, "다시 시도해주세요!", Toast.LENGTH_SHORT).show();

        }

    }

    @Override
    public void onFailure(Call<UserStat> call, Throwable t) {
        //spinKitView.setVisibility(View.INVISIBLE);
        frameLayout.setVisibility(View.INVISIBLE);
        Toast.makeText(SearchStatResultActivity.this, t.getMessage().toString(), Toast.LENGTH_SHORT).show();
    }

}
