package com.suno.bgm.model.youtube;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by suno on 2017. 8. 19..
 */

public class YoutubeSearchResult {
    @SerializedName("items")
    List<SearchData> items;

    public List<SearchData> getItems() {
        return items;
    }

    public void setItems(List<SearchData> items) {
        this.items = items;
    }
}
