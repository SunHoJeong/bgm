package com.suno.bgm.util.db;

/**
 * Created by suno on 2017. 8. 8..
 */

public interface IRecentSearchSchema {
    String USER_TABLE = "recent_search";
    String COLUMN_ID = "_id";
    String COLUMN_SAERCH_ID = "search_id";

    String USER_TABLE_CREATE = "CREATE TABLE IF NOT EXISTS "
            + USER_TABLE
            + " ("
            + COLUMN_ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COLUMN_SAERCH_ID
            + " TEXT NOT NULL)";

    String[] USER_COLUMNS = new String[] {COLUMN_SAERCH_ID};
}
