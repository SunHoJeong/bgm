package com.suno.bgm.model.find_member;

import java.io.Serializable;

/**
 * Created by suno on 2017. 8. 16..
 */

public class FcmBody implements Serializable{
    String fcmTo;
    String title;
    String bodyText;
    BgmUser fcmFrom;

    public FcmBody(){ }

    public FcmBody(String fcmTo, String title, String bodyText, BgmUser fcmFrom) {
        this.fcmTo = fcmTo;
        this.title = title;
        this.bodyText = bodyText;
        this.fcmFrom = fcmFrom;
    }

    public String getFcmTo() {
        return fcmTo;
    }

    public void setFcmTo(String fcmTo) {
        this.fcmTo = fcmTo;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBodyText() {
        return bodyText;
    }

    public void setBodyText(String bodyText) {
        this.bodyText = bodyText;
    }

    public BgmUser getFcmFrom() {
        return fcmFrom;
    }

    public void setFcmFrom(BgmUser fcmFrom) {
        this.fcmFrom = fcmFrom;
    }
}
