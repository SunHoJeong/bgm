package com.suno.bgm.model.find_member;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by suno on 2017. 8. 13..
 */

//TODO: apply builder pattern
public class FindMemberPosting implements Serializable{
    public static final String WRITE_MODE = "writeMode";
    public static final String WRITE_MODE_DUO = "duo";
    public static final String WRITE_MODE_SQUAD = "squad";

    private String author;
    private String authorEmail;
    private String mode;
    private String title;
    private String discordLink;
    private String bodyText;
    private String key;
    private String date;

    public FindMemberPosting(){}

    public FindMemberPosting(String author, String authorEmail, String mode, String title
                             , String discordLink, String bodyText, String date) {
        this.author = author;
        this.authorEmail = authorEmail;
        this.mode = mode;
        this.title = title;
        this.discordLink = discordLink;
        this.bodyText = bodyText;
        this.date = date;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getAuthorEmail() {
        return authorEmail;
    }

    public void setAuthorEmail(String authorEmail) {
        this.authorEmail = authorEmail;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDiscordLink() {
        return discordLink;
    }

    public void setDiscordLink(String discordLink) {
        this.discordLink = discordLink;
    }

    public String getBodyText() {
        return bodyText;
    }

    public void setBodyText(String bodyText) {
        this.bodyText = bodyText;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Map<String, Object> toMap(){
        HashMap<String, Object> result = new HashMap<>();
        result.put("author", author);
        result.put("authorEmail", authorEmail);
        result.put("mode", mode);
        result.put("title", title);
        result.put("discordLink", discordLink);
        result.put("bodyText", bodyText);
        result.put("key", key);
        result.put("date", date);

        return result;
    }
}
