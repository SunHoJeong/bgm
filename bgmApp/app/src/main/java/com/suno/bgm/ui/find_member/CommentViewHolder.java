package com.suno.bgm.ui.find_member;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.suno.bgm.R;
import com.suno.bgm.model.find_member.CommentInfo;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by suno on 2017. 8. 24..
 */

public class CommentViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.textView_comment_user_id)
    TextView tvUserId;
    @BindView(R.id.textView_comment_text)
    TextView tvText;
    @BindView(R.id.textView_comment_date)
    TextView tvDate;
    @BindView(R.id.imageView_comment_user_profile)
    ImageView imgvProfile;

    private Context context;

    public CommentViewHolder(View itemView, Context context) {
        super(itemView);
        this.context = context;

        ButterKnife.bind(this, itemView);
    }

    public void bindView(CommentInfo commentInfo){
        tvUserId.setText(commentInfo.getReqUser().getUserName());
        tvText.setText(commentInfo.getFcmBody().getBodyText());
        tvDate.setText(commentInfo.getDate());
        Glide.with(context).load(commentInfo.getReqUser().getUserProfileUrl()).into(imgvProfile);
    }
}
