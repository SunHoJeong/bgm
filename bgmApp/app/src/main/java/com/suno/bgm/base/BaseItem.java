package com.suno.bgm.base;

/**
 * Created by suno on 2017. 8. 8..
 */

public class BaseItem {
    public static final int TYPE_RECENT_SEARCH = 101;
//    public static final int TYPE_FIND_MEMBER_SOLO = 111;
//    public static final int TYPE_FIND_MEMBER_DUO = 112;
//    public static final int TYPE_FIND_MEMBER_SQUAD = 113;

    private int viewType;

    public BaseItem(int viewType) {
        this.viewType = viewType;
    }

    public int getViewType() {
        return viewType;
    }

    public void setViewType(int viewType) {
        this.viewType = viewType;
    }
}
