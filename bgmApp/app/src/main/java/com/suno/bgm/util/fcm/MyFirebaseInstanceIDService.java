package com.suno.bgm.util.fcm;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.suno.bgm.util.SharedPreferencesService;

/**
 * Created by suno on 2017. 8. 15..
 */

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {
    @Override
    public void onTokenRefresh() {
        String token = FirebaseInstanceId.getInstance().getToken();
        Log.d("MyFCM", "FCM token: " + token);

        SharedPreferencesService sharedPref = SharedPreferencesService.getInstance();
        sharedPref.setPrefData(SharedPreferencesService.KEY_FCM_TOKEN, token);
    }
}