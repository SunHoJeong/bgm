package com.suno.bgm.ui.search_stat.detail;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.suno.bgm.R;
import com.suno.bgm.model.search_stat.Stat;
import com.suno.bgm.model.search_stat.Stat_;
import com.suno.bgm.model.search_stat.UserStat;

import org.parceler.Parcels;
import org.w3c.dom.Text;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by suno on 2017. 8. 7..
 */

public class DetailFragment extends Fragment {
    public static final int GAME_TYPE_SOLO = 100;
    public static final int GAME_TYPE_DUO = 101;
    public static final int GAME_TYPE_SQUAD = 102;

    @BindView(R.id.textView_detail_type_value) TextView tvDetailType;
    @BindView(R.id.textView_detail_rate_value) TextView tvDetailMode;
    @BindView(R.id.textView_detail_win_rate_value) TextView tvDetailWinRate;
    @BindView(R.id.textView_detail_kd_rate_value) TextView tvDetailKdRate;
    @BindView(R.id.textView_detail_best_rating_value) TextView tvDetailBestRating;
    @BindView(R.id.textView_detail_wins_value) TextView tvDetailWins;
    @BindView(R.id.textView_detail_losses_value) TextView tvDetailLosses;
    @BindView(R.id.textView_detail_kills_value) TextView tvDetailKills;
    @BindView(R.id.textView_detail_rating_rank_value) TextView tvDetailRatingRank;
    @BindView(R.id.textView_detail_wins_rank_value) TextView tvDetailWinsRank;
    @BindView(R.id.textView_detail_kills_rank_value) TextView tvDetailKillsRank;
    @BindView(R.id.textView_detail_best_rank_value) TextView tvDetailBestRank;
    @BindView(R.id.textView_detail_wins_points_value) TextView tvDetailWinsPoints;
    @BindView(R.id.textView_detail_round_most_kills_value) TextView tvDetailRoundMostKills;
    @BindView(R.id.textView_detail_daily_kills_value) TextView tvDetailDailyKills;
    @BindView(R.id.textView_detail_weekly_kills_value) TextView tvDetailWeeklyKills;
    @BindView(R.id.textView_detail_most_survival_time_value) TextView tvDetailMostSurvivalTime;

    @BindView(R.id.textView_detail_rounds_played_value) TextView tvDetailRoundsPlayed;
    @BindView(R.id.textView_detail_top10_rate_value) TextView tvDetailTop10Rate;
    @BindView(R.id.textView_detail_avg_dmg_value) TextView tvDetailAvgDmg;
    @BindView(R.id.textView_detail_win_top10_rate_value) TextView tvDetailWinTop10Rate;
    @BindView(R.id.textView_detail_top10s_value) TextView tvDetailTop10s;
    @BindView(R.id.textView_detail_days_value) TextView tvDetailDays;

    @BindView(R.id.textView_detail_top10s_pg_value) TextView tvDetailTop10sPg;
    @BindView(R.id.textView_detail_kills_pg_value) TextView tvDetailKillsPg;
    @BindView(R.id.textView_detail_headshots_kills_rate_pg_value) TextView tvDetailHeadshotPg;
    @BindView(R.id.textView_detail_team_kills_pg_value) TextView tvDetailTeamKillsPg;
    @BindView(R.id.textView_detail_road_kills_pg_value) TextView tvDetailRoadKillsPg;
    @BindView(R.id.textView_detail_revives_pg_value) TextView tvDetailRevivesPg;
    @BindView(R.id.textView_detail_max_kill_streaks_value) TextView tvDetailMaxKillStreaks;
    @BindView(R.id.textView_detail_longest_kill_value) TextView tvDetailLongestKill;
    @BindView(R.id.textView_detail_longest_time_survived_value) TextView tvDetailLongestTimeSurvived;

    private UserStat userStat;
    private int gameType;

    //TODO: args에 너무 큰 data를 저장 -> TransactionTooLargeException 발생!! 해결법은?  Activity.setData(UserStat userstat)?
    public static DetailFragment newInstance(UserStat userStat, int gameType){
        DetailFragment fragment = new DetailFragment();
        Bundle args = new Bundle();
        args.putParcelable(UserStat.class.getName(), Parcels.wrap(userStat));
        args.putInt("gameType", gameType);
        fragment.setArguments(args);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_search_stat_detail, container, false);
        ButterKnife.bind(this, v);

        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle args = getArguments();
        userStat = Parcels.unwrap(args.getParcelable(UserStat.class.getName()));
        gameType = args.getInt("gameType");

        updateDetail(userStat);
    }

    public void updateDetail(UserStat userStat){
        String gType = "";

        switch (gameType){
            case GAME_TYPE_SOLO:
                tvDetailType.setText("솔로게임 상세 정보");
                gType = "solo";
                break;
            case GAME_TYPE_DUO:
                tvDetailType.setText("듀오게임 상세 정보");
                gType = "duo";
                break;
            case GAME_TYPE_SQUAD:
                tvDetailType.setText("스쿼드게임 상세 정보");
                gType= "squad";
                break;
        }

        if(userStat.getStats() != null){
            List<Stat> itemList = userStat.getStats();

            for(Stat stat : itemList){
                if(stat.getRegion().equals("as") && stat.getSeason().equals("2017-pre3") && stat.getMatch().equals(gType)){
                    List<Stat_> item = stat.getStats();

                    for(Stat_ stat_ : item){
                        if(stat_.getLabel().equals("Rating")){
                            tvDetailMode.setText(stat_.getValue());
                            tvDetailRatingRank.setText(stat_.getRank()+"");
                        }
                        else if(stat_.getLabel().equals("Win %")){
                            tvDetailWinRate.setText(stat_.getValue());
                        }
                        else if(stat_.getLabel().equals("K/D Ratio")){
                            tvDetailKdRate.setText(stat_.getValue());
                        }
                        else if(stat_.getLabel().equals("Best Rating")){
                            tvDetailBestRating.setText(stat_.getValue());
                        }
                        else if(stat_.getLabel().equals("Wins")){
                            tvDetailWins.setText(stat_.getValue());
                            tvDetailWinsRank.setText(stat_.getRank()+"");
                        }
                        else if(stat_.getLabel().equals("Losses")){
                            tvDetailLosses.setText(stat_.getValue());
                        }
                        else if(stat_.getLabel().equals("Kills")){
                            tvDetailKills.setText(stat_.getValue());
                            tvDetailKillsRank.setText(stat_.getRank()+"");
                        }
                        else if(stat_.getLabel().equals("Best Rank")) {
                            tvDetailBestRank.setText(stat_.getValue());
                        }
                        else if(stat_.getLabel().equals("Win Points")) {
                            tvDetailWinsPoints.setText(stat_.getValue());
                        }
                        else if(stat_.getLabel().equals("Round Most Kills")) {
                            tvDetailRoundMostKills.setText(stat_.getValue());
                        }
                        else if(stat_.getLabel().equals("Daily Kills")) {
                            tvDetailDailyKills.setText(stat_.getValue());
                        }
                        else if(stat_.getLabel().equals("Weekly Kills")) {
                            tvDetailWeeklyKills.setText(stat_.getValue());
                        }
                        else if(stat_.getLabel().equals("Most Survival Time")) {
                            int minutes = (int) (Math.round(stat_.getValueDec()/60));
                            int seconds =  (int) (Math.round(stat_.getValueDec()%60));

                            tvDetailMostSurvivalTime.setText(String.format("%02d분 %02d초", minutes, seconds));
                        }
                        else if(stat_.getLabel().equals("Rounds Played")){
                            tvDetailRoundsPlayed.setText(stat_.getValue());
                        }
                        else if(stat_.getLabel().equals("Top 10 Rate")){
                            tvDetailTop10Rate.setText(stat_.getValue());
                        }
                        else if(stat_.getLabel().equals("Avg Dmg per Match")){
                            tvDetailAvgDmg.setText(stat_.getValue());
                        }
                        else if(stat_.getLabel().equals("Win Top 10 Ratio")){
                            tvDetailWinTop10Rate.setText(stat_.getValue());
                        }
                        else if(stat_.getLabel().equals("Top 10s")){
                            tvDetailTop10s.setText(stat_.getValue());
                        }
                        else if(stat_.getLabel().equals("tvDetailDays")){
                            tvDetailDays.setText(stat_.getValue());
                        }
                        else if(stat_.getLabel().equals("Days")){
                            tvDetailDays.setText(stat_.getValue());
                        }
                        else if(stat_.getLabel().equals("Top 10s Pg")){
                            tvDetailTop10sPg.setText(stat_.getValue());
                        }
                        else if(stat_.getLabel().equals("Kills Pg")){
                            tvDetailKillsPg.setText(stat_.getValue());
                        }
                        else if(stat_.getLabel().equals("Headshot Kills Pg")){
                            tvDetailHeadshotPg.setText(stat_.getValue());
                        }
                        else if(stat_.getLabel().equals("Team Kills Pg")){
                            tvDetailTeamKillsPg.setText(stat_.getValue());
                        }
                        else if(stat_.getLabel().equals("Road Kills Pg")){
                            tvDetailRoadKillsPg.setText(stat_.getValue());
                        }
                        else if(stat_.getLabel().equals("Revives Pg")){
                            tvDetailRevivesPg.setText(stat_.getValue());
                        }
                        else if(stat_.getLabel().equals("Max Kill Streaks")){
                            tvDetailMaxKillStreaks.setText(stat_.getValue());
                        }
                        else if(stat_.getLabel().equals("Longest Kill")){
                            tvDetailLongestKill.setText(stat_.getValue());
                        }
                        else if(stat_.getLabel().equals("Longest Time Survived")){
                            tvDetailLongestTimeSurvived.setText(stat_.getValue());
                        }

                    }
                }
            }
        }
    }
}
