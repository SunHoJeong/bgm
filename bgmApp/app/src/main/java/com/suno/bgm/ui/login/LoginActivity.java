package com.suno.bgm.ui.login;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.kakao.auth.ErrorCode;
import com.kakao.auth.ISessionCallback;
import android.support.annotation.Nullable;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.kakao.auth.Session;
import com.kakao.network.ErrorResult;
import com.kakao.usermgmt.UserManagement;
import com.kakao.usermgmt.callback.MeResponseCallback;
import com.kakao.usermgmt.response.model.UserProfile;
import com.kakao.util.exception.KakaoException;
import com.kakao.util.helper.log.Logger;
import com.suno.bgm.R;
import com.suno.bgm.model.find_member.BgmUser;
import com.suno.bgm.util.SharedPreferencesService;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.content.ContentValues.TAG;
import static com.kakao.util.helper.Utility.getPackageInfo;

/**
 * Created by suno on 2017. 8. 5..
 */

public class LoginActivity extends Activity {

    private SessionCallback kakaoCallback;
    private SharedPreferencesService prefService;

    private DatabaseReference usersRef;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        prefService = SharedPreferencesService.getInstance();
        usersRef = FirebaseDatabase.getInstance().getReference().child("users");

        //Log.d("MAIN", getKeyHash(this));

        kakaoCallback = new SessionCallback();
        com.kakao.auth.Session.getCurrentSession().addCallback(kakaoCallback);
        com.kakao.auth.Session.getCurrentSession().checkAndImplicitOpen();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Session.getCurrentSession().removeCallback(kakaoCallback);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (Session.getCurrentSession().handleActivityResult(requestCode, resultCode, data)) {
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private class SessionCallback implements ISessionCallback {
        @Override
        public void onSessionOpened() {
            Log.d("TAG" , "세션 오픈됨");
            // 사용자 정보를 가져옴, 회원가입 미가입시 자동가입 시킴
            //redirectSignupActivity();
            requestMe();
            finish();

        }

        @Override
        public void onSessionOpenFailed(KakaoException exception) {
            if(exception != null) {
                Log.d("TAG" , exception.getMessage());
            }
            setContentView(R.layout.activity_login);
        }
    }

    //TODO: delete , Kakao Activity -> LoginActivity로 바꿈
    protected void redirectSignupActivity() { //세션 연결 성공 시 SignupActivity로 넘김
        final Intent intent = new Intent(this, KakaoSignupActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
        finish();
    }

    public static String getKeyHash(final Context context) {
        PackageInfo packageInfo = getPackageInfo(context, PackageManager.GET_SIGNATURES);
        if (packageInfo == null)
            return null;

        for (Signature signature : packageInfo.signatures) {
            try {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                return Base64.encodeToString(md.digest(), Base64.NO_WRAP);
            } catch (NoSuchAlgorithmException e) {
                Log.w(TAG, "Unable to get MessageDigest. signature=" + signature, e);
            }
        }
        return null;
    }

    protected void requestMe() { //유저의 정보를 받아오는 함수
        UserManagement.requestMe(new MeResponseCallback() {
            @Override
            public void onFailure(ErrorResult errorResult) {
                String message = "failed to get user info. msg=" + errorResult;
                Logger.d(message);

                ErrorCode result = ErrorCode.valueOf(errorResult.getErrorCode());
                if (result == ErrorCode.CLIENT_ERROR_CODE) {
                    finish();
                } else {
                    //redirectLoginActivity();
                }
            }

            @Override
            public void onSessionClosed(ErrorResult errorResult) {
                //redirectLoginActivity();
            }

            @Override
            public void onNotSignedUp() {} // 카카오톡 회원이 아닐 시 showSignup(); 호출해야함

            @Override
            public void onSuccess(UserProfile userProfile) {
                //String profileUrl = userProfile.getProfileImagePath();
                //String userId = String.valueOf(userProfile.getId());
                final String userEmail = userProfile.getEmail();
                String userName = userProfile.getNickname();
                String userProfileUrl = userProfile.getProfileImagePath();

                //prefService = SharedPreferencesService.getInstance();

                prefService.setPrefData(SharedPreferencesService.KEY_LOGIN_STATE, true);
                prefService.setPrefData(SharedPreferencesService.KEY_USER_EMAIL, userEmail);
                prefService.setPrefData(SharedPreferencesService.KEY_USER_NAME, userName);
                prefService.setPrefData(SharedPreferencesService.KEY_USER_PROFILE, userProfileUrl);

                final BgmUser user = new BgmUser();
                user.setUserId(userEmail);
                user.setUserName(userName);
                user.setUserProfileUrl(userProfileUrl);
                user.setFcmToken(prefService.getPrefStringData(SharedPreferencesService.KEY_FCM_TOKEN));

                usersRef.child(userEmail.split("\\.")[0]).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if(!dataSnapshot.exists()){
                            Log.d("onDataChange", "email 정보 저장");
                            usersRef.child(userEmail.split("\\.")[0]).push().setValue(user);
                        }
                        else{
                            Log.d("onDataChange", "email 중복");
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.d("onCancelled", databaseError.getMessage());
                    }
                });

            }
        });
    }
}
