package com.suno.bgm.model.search_stat;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

/**
 * Created by suno on 2017. 8. 7..
 */
@Parcel(Parcel.Serialization.BEAN)
public class LiveTracking
{
    @SerializedName("Match")
    Integer match;
    @SerializedName("MatchDisplay")
    String matchDisplay;
    @SerializedName("Season")
    Integer season;
    @SerializedName("RegionId")
    Integer regionId;
    @SerializedName("Region")
    String region;
    @SerializedName("Date")
    String date;
    @SerializedName("Delta")
    Double delta;
    @SerializedName("Value")
    Double value;
    String message;

    public Integer getMatch() {
        return match;
    }

    public void setMatch(Integer match) {
        this.match = match;
    }

    public String getMatchDisplay() {
        return matchDisplay;
    }

    public void setMatchDisplay(String matchDisplay) {
        this.matchDisplay = matchDisplay;
    }

    public Integer getSeason() {
        return season;
    }

    public void setSeason(Integer season) {
        this.season = season;
    }

    public Integer getRegionId() {
        return regionId;
    }

    public void setRegionId(Integer regionId) {
        this.regionId = regionId;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Double getDelta() {
        return delta;
    }

    public void setDelta(Double delta) {
        this.delta = delta;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
