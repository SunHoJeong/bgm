package com.suno.bgm.util.db;

import com.suno.bgm.model.search_stat.RecentSearch;

import java.util.List;

/**
 * Created by suno on 2017. 8. 8..
 */

public interface IRecentSearchDao {
    RecentSearch fetchDataById(int id);
    List<RecentSearch> fetchAllDatas();
    boolean addData(RecentSearch recentSearch);
    boolean addDatas(List<RecentSearch> recentSearchList);
    int deleteData(String id);
    boolean deleteAllDatas();

}
