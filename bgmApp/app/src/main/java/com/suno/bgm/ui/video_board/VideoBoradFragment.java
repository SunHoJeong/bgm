package com.suno.bgm.ui.video_board;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.github.pedrovgs.DraggableListener;
import com.github.pedrovgs.DraggablePanel;
import com.github.ybq.android.spinkit.SpinKitView;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;
import com.suno.bgm.MainActivity;
import com.suno.bgm.R;
import com.suno.bgm.listener.OnItemClickListener;
import com.suno.bgm.model.youtube.SearchData;
import com.suno.bgm.model.youtube.YoutubeSearchResult;
import com.suno.bgm.util.network.YoutubeService;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by suno on 2017. 8. 4..
 */

public class VideoBoradFragment extends Fragment {
    @BindView(R.id.recyclerView_video_list)
    RecyclerView rcvVideoList;
    @BindView(R.id.draggable_panel)
    DraggablePanel draggablePanel;
    @BindView(R.id.spinKitView_video)
    SpinKitView spinKitView;

    public static final String YOUTUBE_API_KEY = "AIzaSyB-dWZibpRffrwZ4-e7Xm3tXSkVnkgU9E8";
    public static final String YOUTUBE_URL = "https://www.googleapis.com/youtube/v3/";

    private List<SearchData> itemList;
    private VideoRecyclerAdapter adapter;
    private Retrofit retrofit;
    private OnItemClickListener listener;

    private YouTubePlayer youtubePlayer;
    private YouTubePlayerSupportFragment youtubeFragment;
    private VideoDetailFragment videoDetailFragment;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_video_board, container, false);
        ButterKnife.bind(this, v);

        ((MainActivity)getActivity()).setToolbarTitle(getResources().getString(R.string.video_board));

        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initYoutubeFragment();
        initDraggablePanel();

        draggablePanel.setVisibility(View.INVISIBLE);

        /*  OnItemClickListenner */
        listener = new OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                youtubePlayer.loadVideo(itemList.get(position).getId().getVideoId());
                draggablePanel.setVisibility(View.VISIBLE);
                draggablePanel.maximize();

                videoDetailFragment.updateView(itemList.get(position));
            }

            @Override
            public void onDeleteClick(int position) {

            }
        };
    }

    public void initRecyclerView(){
        itemList = new ArrayList<>();

        rcvVideoList.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rcvVideoList.setLayoutManager(layoutManager);

        adapter = new VideoRecyclerAdapter(getActivity(), itemList, listener);
        rcvVideoList.setAdapter(adapter);
    }

    public void initView(){
        retrofit = new Retrofit.Builder()
                .client(new OkHttpClient().newBuilder()
                        .addNetworkInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                        .addNetworkInterceptor(new StethoInterceptor()).build())
                .baseUrl(YOUTUBE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        YoutubeService service = retrofit.create(YoutubeService.class);
        Call<YoutubeSearchResult> call = service.getYoutubeList("snippet", "배틀그라운드 하이라이트", YOUTUBE_API_KEY, 20, "date", "video");

        call.enqueue(new Callback<YoutubeSearchResult>() {
            @Override
            public void onResponse(Call<YoutubeSearchResult> call, Response<YoutubeSearchResult> response) {
                Log.d("Youtube", "onResonse -> response code: "+response.code());
                if(response.isSuccessful()){
                    adapter.setItemList(response.body().getItems());
                    itemList = response.body().getItems();

                    spinKitView.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onFailure(Call<YoutubeSearchResult> call, Throwable t) {
                Log.d("Youtube", "onFailure");
                t.printStackTrace();
            }
        });

    }

    private void initYoutubeFragment() {
        youtubeFragment = new YouTubePlayerSupportFragment();
        youtubeFragment.initialize(VideoBoradFragment.YOUTUBE_API_KEY,
                new YouTubePlayer.OnInitializedListener() {
                    @Override
                    public void onInitializationSuccess(YouTubePlayer.Provider provider,
                                                                  YouTubePlayer player, boolean wasRestored) {
                        if (!wasRestored) {
                            Log.d("Youtube", "onInitializationSuccess");
                            youtubePlayer = player;
                            youtubePlayer.setShowFullscreenButton(true);

                            initRecyclerView();
                            initView();
                        }
                    }

                    @Override
                    public void onInitializationFailure(YouTubePlayer.Provider provider,
                                                                  YouTubeInitializationResult error) {
                    }
                });
    }

    private void initDraggablePanel() {
        videoDetailFragment = new VideoDetailFragment();

        draggablePanel.setFragmentManager(getActivity().getSupportFragmentManager());
        draggablePanel.setTopFragment(youtubeFragment);
        draggablePanel.setBottomFragment(videoDetailFragment);

        draggablePanel.setDraggableListener(new DraggableListener() {
            @Override
            public void onMaximized() {
                Log.d("DraggablePanel", "onMaximized");
                playVideo();
            }

            @Override
            public void onMinimized() {
                Log.d("DraggablePanel", "onMinimized");
                //Empty
            }

            @Override
            public void onClosedToLeft() {
                Log.d("DraggablePanel", "onClosedToLeft");
                pauseVideo();
            }

            @Override
            public void onClosedToRight() {
                Log.d("DraggablePanel", "onClosedRight");
                pauseVideo();
            }
        });

        draggablePanel.initializeView();
    }

    private void pauseVideo() {
        if (youtubePlayer.isPlaying()) {
            youtubePlayer.pause();
        }
    }

    private void playVideo() {
        if (!youtubePlayer.isPlaying()) {
            youtubePlayer.play();
        }
    }
}
