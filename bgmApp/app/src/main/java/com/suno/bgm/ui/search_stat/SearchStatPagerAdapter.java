package com.suno.bgm.ui.search_stat;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.suno.bgm.model.search_stat.UserStat;
import com.suno.bgm.ui.DefaultFragment;
import com.suno.bgm.ui.search_stat.overview.OverviewFragment;
import com.suno.bgm.ui.search_stat.detail.DetailFragment;

/**
 * Created by suno on 2017. 8. 4..
 */

public class SearchStatPagerAdapter extends FragmentPagerAdapter {
    private int tabCount;
    private UserStat userStat;

    public SearchStatPagerAdapter(FragmentManager fm, int tabCount, UserStat userStat) {
        super(fm);
        this.tabCount = tabCount;
        this.userStat = userStat;
    }

    @Override
    public Fragment getItem(int position) {
        switch(position){
            case 0:
                return OverviewFragment.newInstance(userStat);
            case 1:
                return DetailFragment.newInstance(userStat, DetailFragment.GAME_TYPE_SOLO);
            case 2:
                return DetailFragment.newInstance(userStat, DetailFragment.GAME_TYPE_DUO);
            case 3:
                return DetailFragment.newInstance(userStat, DetailFragment.GAME_TYPE_SQUAD);
            default:
                return new DefaultFragment();
        }
    }

    @Override
    public int getCount() {
        return tabCount;
    }
}
