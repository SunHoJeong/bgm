package com.suno.bgm.model;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by suno on 2017. 8. 2..
 */

public class SampleImageLocalDataSource implements SampleImageSource {
//    private SampleImageLocalDataSource(){}
//    private static SampleImageLocalDataSource sampleImageLocalDataSource;
//
//    public static SampleImageLocalDataSource getInstance(){
//        if(sampleImageLocalDataSource == null){
//            sampleImageLocalDataSource = new SampleImageLocalDataSource();
//        }
//        return sampleImageLocalDataSource;
//    }
//
//    public List<ImageItem> getItems(Context context, int size){
//        List<ImageItem> itemList = new ArrayList<>();
//
//        return itemList;
//    }

    @Override
    public void getImages (Context context, int size, LoadImageCallback loadImageCallback) {
        ArrayList<ImageItem> items = new ArrayList<>();

        if(loadImageCallback != null){
            loadImageCallback.onImageLoaded(items);
        }
    }
}
