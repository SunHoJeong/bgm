package com.suno.bgm.ui.find_member;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bowyer.app.fabtransitionlayout.BottomSheetLayout;
import com.github.pedrovgs.DraggableListener;
import com.github.pedrovgs.DraggablePanel;
import com.github.ybq.android.spinkit.SpinKitView;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.suno.bgm.MainActivity;
import com.suno.bgm.R;
import com.suno.bgm.listener.OnItemClickListener;
import com.suno.bgm.model.find_member.FindMemberPosting;
import com.suno.bgm.util.MessageEvent;
import com.suno.bgm.util.SharedPreferencesService;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.google.android.gms.internal.zzs.TAG;

/**
 * Created by suno on 2017. 8. 4..
 */

public class FindMemberFragment extends Fragment
        implements ChildEventListener, OnItemClickListener {
    @BindView(R.id.fb_button)
    FloatingActionButton faBtn;
    @BindView(R.id.bottom_sheet)
    BottomSheetLayout bsLayout;
    @BindView(R.id.recyclerView_find_member)
    RecyclerView rcvFindMember;
    @BindView(R.id.spinKitView_find_memeber)
    SpinKitView spinKitView;
    @BindView(R.id.draggable_panel_posting)
    DraggablePanel draggablePanel;

    private static final String TAG = "FindMemberFragment";
    private SharedPreferencesService sharedPref;
    private PostingAdapter adapter;
    private List<String> itemIdList;
    private List<FindMemberPosting> itemList;
    private DatabaseReference postingRef;

    private PostingTopFragment topFragment;
    private PostingBottomFragment bottomFragment;

    private static FindMemberFragment instance = null;

    public static FindMemberFragment getInstance() {
        if (instance == null) {
            instance = new FindMemberFragment();
        }

        return instance;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_find_member, container, false);
        ButterKnife.bind(this, v);

        ((MainActivity) getActivity()).setToolbarTitle(getResources().getString(R.string.find_member));
        bsLayout.setFab(faBtn);
        Log.d("FindMember", "onCreateView");
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        sharedPref = SharedPreferencesService.getInstance();
        firebaseDbInit();
        EventBus.getDefault().register(this);

        initRecyclerView();
        initDraggablePanel();
        spinKitView.setVisibility(View.VISIBLE);
        Log.d("FindMember", "onVIewCreated  ");
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("FindMember", "onCreate");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("FindMember", "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d("FindMember", "onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d("FindMember", "onStop");
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d("FindMember", "onActivityCreated");

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("FindMember", "onDistroy");
        postingRef.child("posting").removeEventListener(this);
        EventBus.getDefault().unregister(this);
        //adapter.cleanup();
    }

    @OnClick(R.id.fb_button)
    public void faBtnClick(View v) {
        bsLayout.expandFab();
    }

    @OnClick({R.id.bottom_sheet_item_two, R.id.bottom_sheet_item_three})
    public void bottomItemClick(View v) {
        switch (v.getId()) {
            case R.id.bottom_sheet_item_two:
                redirectWritePostingActivity(FindMemberPosting.WRITE_MODE_DUO);
                break;
            case R.id.bottom_sheet_item_three:
                redirectWritePostingActivity(FindMemberPosting.WRITE_MODE_SQUAD);
                break;
        }
    }

    public void redirectWritePostingActivity(String mode) {
        Intent intent = new Intent(getActivity(), WritePostingActivity.class);
        intent.putExtra(FindMemberPosting.WRITE_MODE, mode);
        startActivity(intent);
    }

    public void firebaseDbInit() {
        postingRef = FirebaseDatabase.getInstance().getReference();

        postingRef.child("posting").addChildEventListener(this);
        postingRef.child("posting").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.d("FM DataChange", "Data loading end!");
                spinKitView.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void initRecyclerView() {
        itemIdList = new ArrayList<>();
        itemList = new ArrayList<>();

        rcvFindMember.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rcvFindMember.setLayoutManager(layoutManager);
        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true);

        adapter = new PostingAdapter(getActivity(), itemList, this);
        rcvFindMember.setAdapter(adapter);
    }

    public void initDraggablePanel() {
        draggablePanel.setVisibility(View.INVISIBLE);

        topFragment = new PostingTopFragment();
        bottomFragment = new PostingBottomFragment();

        draggablePanel.setFragmentManager(getActivity().getSupportFragmentManager());
        draggablePanel.setTopFragment(topFragment);
        draggablePanel.setBottomFragment(bottomFragment);

        draggablePanel.setDraggableListener(new DraggableListener() {
            @Override
            public void onMaximized() {
                faBtn.setVisibility(View.INVISIBLE);
                Log.d("DraggablePanel", "onMaximized");
            }

            @Override
            public void onMinimized() {
                faBtn.setVisibility(View.VISIBLE);
                Log.d("DraggablePanel", "onMinimized");
            }

            @Override
            public void onClosedToLeft() {
                Log.d("DraggablePanel", "onClosedToLeft");
            }

            @Override
            public void onClosedToRight() {
                Log.d("DraggablePanel", "onClosedRight");
            }
        });

        draggablePanel.initializeView();
    }

    //TODO: 가끔 다수의 게시물이 올라감 왜일까...?
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent massage) {
        FindMemberPosting fmPosting = massage.getFbPosting();

        String key = postingRef.child("posting").push().getKey();
        fmPosting.setKey(key);

        Map<String, Object> postValues = fmPosting.toMap();
        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/posting/" + key, postValues);
        childUpdates.put("/user-posts/" + fmPosting.getAuthorEmail().split("\\.")[0] + "/" + key, postValues);

        postingRef.updateChildren(childUpdates);

        Log.d("FM Subscribe", fmPosting.getTitle() + "," + fmPosting.getBodyText() + ",key" + key);
    }

    /* ChildEventListener */
    @Override
    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
        FindMemberPosting fmPosting = dataSnapshot.getValue(FindMemberPosting.class);

        Log.d("FM posting onchildAdded", fmPosting.getTitle() + "," + fmPosting.getBodyText() + ", key: " + dataSnapshot.getKey());
        itemIdList.add(dataSnapshot.getKey());
        itemList.add(fmPosting);
        adapter.notifyItemInserted(itemList.size() - 1);

        makeSnackBar();
    }

    public void makeSnackBar() {
        Snackbar.make(getActivity().findViewById(android.R.id.content), "글 목록이 업데이트 되었습니다", Snackbar.LENGTH_SHORT)
                .setAction("확인", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                }).show();
    }

    @Override
    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
        FindMemberPosting newPosting = dataSnapshot.getValue(FindMemberPosting.class);
        String postingKey = dataSnapshot.getKey();

        int postingIdx = itemIdList.indexOf(postingKey);
        if (postingIdx > -1) {
            itemList.set(postingIdx, newPosting);

            adapter.notifyItemChanged(postingIdx);
        } else {
            Log.w(TAG, "onChildChanged:unknown_child:" + postingKey);
        }

        Log.d("FM posting onChanged", newPosting.getTitle() + "," + newPosting.getBodyText());
    }

    @Override
    public void onChildRemoved(DataSnapshot dataSnapshot) {
        String postingKey = dataSnapshot.getKey();

        int postingIdx = itemIdList.indexOf(postingKey);
        if (postingIdx > -1) {
            itemIdList.remove(postingIdx);
            itemList.remove(postingIdx);

            adapter.notifyItemRemoved(postingIdx);
        } else {
            Log.w(TAG, "onChildChanged:unknown_child:" + postingKey);
        }
    }

    @Override
    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

    }

    @Override
    public void onCancelled(DatabaseError databaseError) {

    }

    /* OnItemClickListener */
    @Override
    public void onItemClick(int position) {

        Log.d("onItemClick->", position + ", " + itemList.get(position).getBodyText() + "/ " + itemList.get(position).getDiscordLink());
        topFragment.updateView(itemList.get(position));
        bottomFragment.setPosting(itemList.get(position));

        draggablePanel.setVisibility(View.VISIBLE);
        draggablePanel.maximize();

    }

    @Override
    public void onDeleteClick(int position) {

    }

}
