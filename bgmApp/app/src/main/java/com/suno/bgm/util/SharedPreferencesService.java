package com.suno.bgm.util;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by suno on 2017. 8. 14..
 */

public class SharedPreferencesService {
    public static final String KEY_USER_EMAIL = "userEmail";
    public static final String KEY_USER_NAME = "userName";
    public static final String KEY_LOGIN_STATE = "loginState";
    public static final String KEY_USER_PROFILE = "userProfile";
    public static final String KEY_FCM_TOKEN = "fcmToken";
    public static final String KEY_CALL_ACTIVITY = "callActivity";
    public static final String KEY_POST_TITLE = "postTitle";
    public static final String KEY_POST_TEXT_BODY = "postTextBody";

    private static final String SHARED_PREFS_CONFIGURATION = "BGMConfiguration";

    private volatile static SharedPreferencesService sharedPreferencesManager;
    private SharedPreferences pref;

    public static SharedPreferencesService getInstance() {
        if (sharedPreferencesManager == null) {
            synchronized (SharedPreferencesService.class) {
                if (sharedPreferencesManager == null)
                    sharedPreferencesManager = new SharedPreferencesService();
            }
        }
        return sharedPreferencesManager;
    }

    /**
     * 공유설정 정보를 취득한다.
     */
    private void getPref(Context cont) {
        if (pref == null) {
            pref = cont.getSharedPreferences(SHARED_PREFS_CONFIGURATION, Context.MODE_PRIVATE);
        }
    }

    public void load(Context context) {
        getPref(context);
    }

    public long getPrefLongData(String key) {
        return getPrefLongData(key, 0);
    }

    public long getPrefLongData(String key, long defValue) {
        return pref.getLong(key, defValue);
    }

    public int getPrefIntegerData(String key) {
        return getPrefIntegerData(key, 0);
    }

    public int getPrefIntegerData(String key, int defValue) {
        return pref.getInt(key, defValue);
    }

    public float getPrefFloatData(String key) {
        return getPrefFloatData(key, 0.f);
    }

    public float getPrefFloatData(String key, float defValue) {
        return pref.getFloat(key, defValue);
    }


    public String getPrefStringData(String key, String defValue) {
        return pref.getString(key, defValue);
    }

    public String getPrefStringData(String key) {
        return getPrefStringData(key, "");
    }

    public boolean getPrefBooleanData(String key, boolean defValue) {
        return pref.getBoolean(key, defValue);
    }


    public boolean getPrefBooleanData(String key) {
        return getPrefBooleanData(key, false);
    }

    public void setPrefData(String key, boolean value) {
        SharedPreferences.Editor editor = pref.edit();

        editor.putBoolean(key, value);
        editor.commit();
    }

    public void setPrefData(String key, int value) {
        SharedPreferences.Editor editor = pref.edit();

        editor.putInt(key, value);
        editor.commit();
    }

    public void setPrefData(String key, float value) {
        SharedPreferences.Editor editor = pref.edit();

        editor.putFloat(key, value);
        editor.commit();
    }

    public void setPrefData(String key, long value) {
        SharedPreferences.Editor editor = pref.edit();

        editor.putLong(key, value);
        editor.commit();
    }

    public void setPrefData(String key, String value) {
        SharedPreferences.Editor editor = pref.edit();

        editor.putString(key, value);
        editor.commit();
    }

    public void removeData(String... keys) {
        SharedPreferences.Editor editor = pref.edit();

        for (String key : keys) {
            editor.remove(key);
        }

        editor.commit();

    }
}