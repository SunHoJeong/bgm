package com.suno.bgm.model.find_member;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import static android.R.attr.author;
import static android.R.attr.mode;

/**
 * Created by suno on 2017. 8. 21..
 */

public class CommentInfo implements Serializable {
    private FcmBody fcmBody;
    private BgmUser reqUser;
    private BgmUser writerUser;
    private String date;
    private String key;
    private String postKey;

    public CommentInfo(){}

    public CommentInfo(FcmBody fcmBody, BgmUser reqUser, BgmUser writerUser, String date, String key, String postKey) {
        this.fcmBody = fcmBody;
        this.reqUser = reqUser;
        this.writerUser = writerUser;
        this.date = date;
        this.key = key;
        this.postKey = postKey;
    }

    public FcmBody getFcmBody() {
        return fcmBody;
    }

    public void setFcmBody(FcmBody fcmBody) {
        this.fcmBody = fcmBody;
    }

    public BgmUser getReqUser() {
        return reqUser;
    }

    public void setReqUser(BgmUser reqUser) {
        this.reqUser = reqUser;
    }

    public BgmUser getWriterUser() {
        return writerUser;
    }

    public void setWriterUser(BgmUser writerUser) {
        this.writerUser = writerUser;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getPostKey() {
        return postKey;
    }

    public void setPostKey(String postKey) {
        this.postKey = postKey;
    }

    public Map<String, Object> toMap(){
        HashMap<String, Object> result = new HashMap<>();
        result.put("fcmBody", fcmBody);
        result.put("reqUser", reqUser);
        result.put("writerUser", writerUser);
        result.put("date", date);
        result.put("key", key);
        result.put("postKey", postKey);

        return result;
    }
}
