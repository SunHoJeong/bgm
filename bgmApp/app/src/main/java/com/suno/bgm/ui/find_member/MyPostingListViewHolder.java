package com.suno.bgm.ui.find_member;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.suno.bgm.R;
import com.suno.bgm.listener.OnItemClickListener;
import com.suno.bgm.model.find_member.FindMemberPosting;
import com.suno.bgm.util.TimeHelper;

import org.w3c.dom.Text;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by suno on 2017. 8. 21..
 */

public class MyPostingListViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.textView_my_post_title)
    TextView tvTitle;
    @BindView(R.id.textView_my_post_mode)
    TextView tvMode;
    @BindView(R.id.textView_my_post_date)
    TextView tvDate;

    private Context context;
    private OnItemClickListener listener;

    public MyPostingListViewHolder(View itemView, Context context) {
        super(itemView);
        this.context = context;

        ButterKnife.bind(this, itemView);
    }

    public void bindView(FindMemberPosting posting){
        switch (posting.getMode()){
            case FindMemberPosting.WRITE_MODE_DUO :
                tvMode.setText(context.getResources().getString(R.string.duo));
                break;
            case FindMemberPosting.WRITE_MODE_SQUAD :
                tvMode.setText(context.getResources().getString(R.string.squad));
                break;
            default:
                tvMode.setText(posting.getMode()+"");
        }

        tvTitle.setText(posting.getTitle());

        String date = posting.getDate();
        SimpleDateFormat transFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            tvDate.setText(TimeHelper.getElapsedDateString(transFormat.parse(date)));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public void setOnItemClickListener(OnItemClickListener listener){
        this.listener = listener;
    }

    @OnClick(R.id.linearLayout_my_posting)
    public void onItemClick(View v){
        listener.onItemClick(getAdapterPosition());
    }

}
