package com.suno.bgm.ui.search_stat;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.suno.bgm.R;
import com.suno.bgm.base.BaseItem;
import com.suno.bgm.listener.OnItemClickListener;
import com.suno.bgm.model.search_stat.RecentSearch;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by suno on 2017. 8. 8..
 */

public class RecentSearchViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.textView_recent_search_id)
    TextView tvRecentSearchId;
    @BindView(R.id.imageButton_recent_search_delete)
    ImageButton imgBtnDelete;

    private Context context;
    private RecentSearch recentSearch;
    private OnItemClickListener listener;

    public RecentSearchViewHolder(View itemView, Context context) {
        super(itemView);
        this.context = context;

        ButterKnife.bind(this, itemView);

    }

    public void bindView(BaseItem item) {
        recentSearch = ((RecentSearch) item);
        tvRecentSearchId.setText(recentSearch.getSearchId());
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @OnClick({R.id.textView_recent_search_id, R.id.imageButton_recent_search_delete})
    public void onItemClick(View v) {
        switch (v.getId()) {
            case R.id.textView_recent_search_id:
                listener.onItemClick(getAdapterPosition());
                break;

            case R.id.imageButton_recent_search_delete:
                listener.onDeleteClick(getAdapterPosition());
                break;
        }

    }


}