package com.suno.bgm.ui.search_stat.overview;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.suno.bgm.R;
import com.suno.bgm.model.search_stat.MatchHistory;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by suno on 2017. 8. 10..
 */

public class OverviewViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.textView_match_top10)
    TextView tvMatchTop10;
    @BindView(R.id.textView_match_mode)
    TextView tvMatchMode;
    @BindView(R.id.textView_match_rating_value)
    TextView tvMatchRating;
    @BindView(R.id.textView_match_kill_value)
    TextView tvMatchKill;
    @BindView(R.id.textView_match_assist_value)
    TextView tvMatchAssist;
    @BindView(R.id.textView_match_damage_value)
    TextView tvMatchDamage;
    @BindView(R.id.textView_match_distance_value)
    TextView tvMatchDistance;
    @BindView(R.id.textView_match_survive_value)
    TextView tvMatchSurvive;

    private Context context;

    private MatchHistory matchHistory;

    public OverviewViewHolder(View itemView, Context context) {
        super(itemView);
        this.context = context;

        ButterKnife.bind(this, itemView);
    }

    public void bindView(MatchHistory matchHistory){
        this.matchHistory = matchHistory;

        updateMatchHistory(matchHistory);
    }

    //TODO 정교하게 다듬기
    public void updateMatchHistory(MatchHistory matchHistory){
        if(matchHistory.getTop10() == 1){
            if(matchHistory.getWins() == 1){
                tvMatchTop10.setText("승리");
                tvMatchTop10.setTextColor(context.getResources().getColor(R.color.colorWin));
            }
            else{
                tvMatchTop10.setText("Top 10");
                tvMatchTop10.setTextColor(context.getResources().getColor(R.color.colorTop10));
            }
        }
        else{
            tvMatchTop10.setText(String.valueOf(matchHistory.getMatch()) +" 매치");
            tvMatchTop10.setTextColor(context.getResources().getColor(R.color.colorMatch));
        }

        switch (matchHistory.getMatch()){
            case 1 :
                tvMatchMode.setText("솔로");
                break;
            case 2 :
                tvMatchMode.setText("듀오");
                break;
            case 3 :
                tvMatchMode.setText("스쿼드");
                break;
        }

        tvMatchRating.setText(String.valueOf(Math.round(matchHistory.getRating())));
        tvMatchKill.setText(String.valueOf(matchHistory.getKills()));
        tvMatchAssist.setText(String.valueOf(matchHistory.getAssists()));
        tvMatchDamage.setText(String.valueOf(matchHistory.getDamage()));

        int minutes = (int) (Math.round(matchHistory.getTimeSurvived()/60));
        int seconds =  (int) (Math.round(matchHistory.getTimeSurvived()%60));

        tvMatchSurvive.setText(String.format("%02d분 %02d초", minutes, seconds));
        tvMatchDistance.setText(String.format("%.2f", matchHistory.getMoveDistance()/1000)+"km");

    }
}
