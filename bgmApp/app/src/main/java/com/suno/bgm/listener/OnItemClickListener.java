package com.suno.bgm.listener;

/**
 * Created by suno on 2017. 8. 2..
 */

public interface OnItemClickListener {
    void onDeleteClick(int position);
    void onItemClick(int position);
}
