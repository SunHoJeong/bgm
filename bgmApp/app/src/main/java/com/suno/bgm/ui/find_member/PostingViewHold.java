package com.suno.bgm.ui.find_member;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.suno.bgm.R;
import com.suno.bgm.listener.OnItemClickListener;
import com.suno.bgm.model.find_member.FindMemberPosting;
import com.suno.bgm.util.TimeHelper;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by suno on 2017. 8. 13..
 */

public class PostingViewHold extends RecyclerView.ViewHolder {
    @BindView(R.id.textView_find_member_mode)
    TextView tvMode;
    @BindView(R.id.textView_find_member_title)
    TextView tvTitle;
    @BindView(R.id.textView_find_member_date)
    TextView tvDate;
    @BindView(R.id.textView_find_member_user_id)
    TextView tvUserId;

    private Context context;
    private OnItemClickListener listener;

    public PostingViewHold(View itemView, Context context) {
        super(itemView);
        this.context = context;

        ButterKnife.bind(this, itemView);
    }

    public void bindView(FindMemberPosting fmPosting){
        Log.d("FM ViewHolder", fmPosting.getTitle());
        switch (fmPosting.getMode()){
            case FindMemberPosting.WRITE_MODE_DUO :
                tvMode.setText(context.getResources().getString(R.string.duo));
                break;
            case FindMemberPosting.WRITE_MODE_SQUAD :
                tvMode.setText(context.getResources().getString(R.string.squad));
                break;
            default:
                tvMode.setText(fmPosting.getMode()+"");
        }

        tvTitle.setText(fmPosting.getTitle());
        tvUserId.setText(fmPosting.getAuthor());

        String date = fmPosting.getDate();
        SimpleDateFormat transFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            tvDate.setText(TimeHelper.getElapsedDateString(transFormat.parse(date)));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @OnClick(R.id.relativeLayout_find_member_posting)
    public void itemClick(View v){
        listener.onItemClick(getAdapterPosition());
    }
}
