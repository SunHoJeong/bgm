package com.suno.bgm.ui.find_member;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.github.ybq.android.spinkit.SpinKitView;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.suno.bgm.R;
import com.suno.bgm.application.BgmApplication;
import com.suno.bgm.model.find_member.BgmUser;
import com.suno.bgm.model.find_member.CommentInfo;
import com.suno.bgm.model.find_member.FcmBody;
import com.suno.bgm.model.find_member.FindMemberPosting;
import com.suno.bgm.util.SharedPreferencesService;
import com.suno.bgm.util.network.HttpService;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.R.attr.key;
import static com.suno.bgm.util.SharedPreferencesService.KEY_USER_EMAIL;

/**
 * Created by suno on 2017. 8. 24..
 */

public class PostingBottomFragment extends Fragment {
    @BindView(R.id.recyclerView_comment)
    RecyclerView rcvComment;
    @BindView(R.id.editText_comment)
    EditText etComment;
    @BindView(R.id.spinKitView_comment)
    SpinKitView spinKitView;

    private SharedPreferencesService sharedPref;
    private CommentAdapter adapter;
    private List<CommentInfo> commentList;
    private FindMemberPosting posting;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_posting_bottom, container, false);
        ButterKnife.bind(this, v);

        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        spinKitView.setVisibility(View.INVISIBLE);
        sharedPref = SharedPreferencesService.getInstance();

        initRecyclerView();

    }

    public void initRecyclerView(){
        commentList = new ArrayList<>();
        adapter = new CommentAdapter(commentList, getActivity());

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        rcvComment.setHasFixedSize(true);
        rcvComment.setLayoutManager(layoutManager);
        rcvComment.setAdapter(adapter);
    }

    public void setPosting(FindMemberPosting posting){
        this.posting = posting;

        updateView();
    }

    public void updateView(){
        spinKitView.setVisibility(View.VISIBLE);

        if(!commentList.isEmpty()){
            commentList.clear();
            adapter.notifyDataSetChanged();
        }

        String postingKey = posting.getKey();

        DatabaseReference commentRef = FirebaseDatabase.getInstance().getReference();
        commentRef.child("comment").child(postingKey).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                CommentInfo commentInfo = dataSnapshot.getValue(CommentInfo.class);

                //adapter.addDate(commentInfo);
                //int position = commentList.size();
                commentList.add(commentInfo);
                adapter.notifyItemInserted(commentList.size()-1);
                //adapter.notifyDataSetChanged();
                Log.d("onChildAdded-comment", commentList.size()-1 +","+commentInfo.getFcmBody().getBodyText());
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        commentRef.child("comment").child(postingKey).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                spinKitView.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @OnClick(R.id.image_button_send)
    public void sendClick(View v){
        final String comment = etComment.getText().toString();
        etComment.setText("");

        final DatabaseReference postRef = FirebaseDatabase.getInstance().getReference()
                .child("users").child(posting.getAuthorEmail().split("\\.")[0]);

        Log.d("RETRO1", "글쓴이:"+posting.getAuthorEmail().split("\\.")[0]);

        final List<BgmUser> writerUser = new ArrayList<>();
        //search posting writer
        postRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot ds : dataSnapshot.getChildren()){
                    writerUser.add(ds.getValue(BgmUser.class));
                }

                final String postingKey = posting.getKey();
                final List<BgmUser> reqUser = new ArrayList<>();

                //search posting req user
                DatabaseReference userRef = FirebaseDatabase.getInstance().getReference();
                String applyerEmail = sharedPref.getPrefStringData(KEY_USER_EMAIL);
                userRef.child("users").child(applyerEmail.split("\\.")[0])
                        .addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                for(DataSnapshot ds : dataSnapshot.getChildren()){
                                    reqUser.add(ds.getValue(BgmUser.class));
                                }
                                Log.d("searchBgmUser", reqUser.get(0).getUserId());

                                DatabaseReference commentRef = FirebaseDatabase.getInstance().getReference();

                                //push fcm reqUser -> writerUser
                                FcmBody fcmToWriter = new FcmBody(writerUser.get(0).getFcmToken(), posting.getTitle()
                                        , comment, reqUser.get(0));
                                pushFcm(fcmToWriter);

                                Date currentTime = Calendar.getInstance().getTime();
                                SimpleDateFormat transFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                String date = transFormat.format(currentTime);

                                String nextKey = commentRef.child("comment").child(postingKey).push().getKey();
                                CommentInfo commentInfo = new CommentInfo(fcmToWriter, reqUser.get(0), writerUser.get(0), date, nextKey, postingKey);

                                Map<String, Object> commentValue = commentInfo.toMap();
                                Map<String, Object> childUpdates = new HashMap<>();

                                childUpdates.put("/comment/" + postingKey +"/"+ nextKey, commentValue);
                                commentRef.updateChildren(childUpdates);
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                            }
                        });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void pushFcm(FcmBody fcmBody){
        HttpService service = BgmApplication.getRetrofit().create(HttpService.class);
        Call<String> call = service.postFcmPushToWriter(fcmBody);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                int statusCode = response.code();
                Log.d("RETRO_ENQUEUE", statusCode+"");
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d("RETRO_ENQUEUE", "error: "+t.toString());
            }
        });

    }
}
