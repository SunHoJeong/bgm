package com.suno.bgm.ui.info_board;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.suno.bgm.MainActivity;
import com.suno.bgm.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by suno on 2017. 8. 4..
 */

public class InfoBoradFragment extends Fragment {
    @BindView(R.id.webView)
    WebView webView;

    private final String MAP_URL = "https://pubgmap.io/ko/erangel.html#3/-127.00/127.00";
    private WebSettings webSettings;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_info_board, container, false);
        ButterKnife.bind(this, v);

        ((MainActivity)getActivity()).setToolbarTitle(getResources().getString(R.string.info_board));

        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        webView.loadUrl(MAP_URL);

    }
}
