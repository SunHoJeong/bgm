package com.suno.bgm.ui.find_member;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.suno.bgm.MainActivity;
import com.suno.bgm.R;
import com.suno.bgm.model.find_member.FindMemberPosting;
import com.suno.bgm.util.MessageEvent;
import com.suno.bgm.util.SharedPreferencesService;

import org.greenrobot.eventbus.EventBus;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.suno.bgm.model.find_member.FindMemberPosting.WRITE_MODE;

/**
 * Created by suno on 2017. 8. 12..
 */

public class WritePostingActivity extends AppCompatActivity {

    @BindView(R.id.toolbar_write_post)
    Toolbar toolbar;
    @BindView(R.id.textView_write_post_title)
    TextView tvToolbarTitle;
    @BindView(R.id.editText_write_post_title)
    EditText etTitle;
    @BindView(R.id.editText_write_post_discord_link)
    EditText etDiscordLink;
    @BindView(R.id.editText_write_post_body_text)
    EditText etBodyText;

    private SharedPreferencesService sharedPref;
    private String mode;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPref = SharedPreferencesService.getInstance();

        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();

        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if ("text/plain".equals(type)) {
                String calledActivity = sharedPref.getPrefStringData(SharedPreferencesService.KEY_CALL_ACTIVITY);

                setContentView(R.layout.activity_write_post);

                ButterKnife.bind(this);
                mode = calledActivity;
                handleRecievedText(intent);
            }
        }
        else{
            Log.d("WritePosting", action + ",," + type);
            setContentView(R.layout.activity_write_post);
            mode = getDataFromIntent();
            ButterKnife.bind(this);
        }

        initToolbar();
    }

    void handleRecievedText(Intent intent) {
        String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
        if (sharedText != null) {
            etTitle.setText(sharedPref.getPrefStringData(SharedPreferencesService.KEY_POST_TITLE));
            etBodyText.setText(sharedPref.getPrefStringData(SharedPreferencesService.KEY_POST_TEXT_BODY));
            etDiscordLink.setText(sharedText);
            // Update UI to reflect text being shared
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void initToolbar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if(mode != null){
            tvToolbarTitle.setText(mode);
        }
        else{
            tvToolbarTitle.setText(sharedPref.getPrefStringData(SharedPreferencesService.KEY_CALL_ACTIVITY));
        }

    }

    public String getDataFromIntent(){
        Intent intent = getIntent();

        return intent.getStringExtra(WRITE_MODE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.button_write_post_submit)
    public void submitClick(View v){
        String title = etTitle.getText().toString();
        String discordLink = etDiscordLink.getText().toString();
        String bodyText = etBodyText.getText().toString();

        String userEmail = sharedPref.getPrefStringData(SharedPreferencesService.KEY_USER_EMAIL, "디폴트 Email");
        String userName = sharedPref.getPrefStringData(SharedPreferencesService.KEY_USER_NAME, "디폴트 Name");
        Boolean loginState = sharedPref.getPrefBooleanData(SharedPreferencesService.KEY_LOGIN_STATE, false);

        Date currentTime = Calendar.getInstance().getTime();
        SimpleDateFormat transFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        FindMemberPosting posting = new FindMemberPosting(userName, userEmail, mode, title, discordLink
                , bodyText, transFormat.format(currentTime));

        EventBus.getDefault().post(new MessageEvent(posting));

        finish();
        redirectMainActivity();
    }

    @OnClick(R.id.button_open_discord)
    public void discordClick(View v){
        String discordName = "com.discord";

        if(mode.equals(FindMemberPosting.WRITE_MODE_DUO)){
            sharedPref.setPrefData(SharedPreferencesService.KEY_CALL_ACTIVITY, FindMemberPosting.WRITE_MODE_DUO);
        }
        else{
            sharedPref.setPrefData(SharedPreferencesService.KEY_CALL_ACTIVITY, FindMemberPosting.WRITE_MODE_SQUAD);
        }

        if (isPackageInstalled(discordName, this)) {
            sharedPref.setPrefData(SharedPreferencesService.KEY_POST_TITLE, etTitle.getText().toString());
            sharedPref.setPrefData(SharedPreferencesService.KEY_POST_TEXT_BODY, etBodyText.getText().toString());

            Intent intent = this.getPackageManager().getLaunchIntentForPackage(discordName);
            this.startActivity(intent);
        }
        else {
            String url = "market://details?id=" + discordName;
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            this.startActivity(intent);
        }
    }

    private boolean isPackageInstalled(String packageName, Context context) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public void redirectMainActivity(){
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(MainActivity.FRAGMENT_FLAG, MainActivity.FRAGMENT_FIND_MEMBER );
        startActivity(intent);
    }

}
