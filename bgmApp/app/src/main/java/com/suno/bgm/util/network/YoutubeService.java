package com.suno.bgm.util.network;

import com.suno.bgm.model.youtube.YoutubeSearchResult;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

import static android.R.attr.type;

/**
 * Created by suno on 2017. 8. 19..
 */

public interface YoutubeService {
    @GET("search")
    Call<YoutubeSearchResult> getYoutubeList(@Query("part") String part,
                                             @Query("q") String keyword,
                                             @Query("key") String apiKey,
                                             @Query("maxResults") int resultCount,
                                             @Query("order") String order,
                                             @Query("type") String type);
}
