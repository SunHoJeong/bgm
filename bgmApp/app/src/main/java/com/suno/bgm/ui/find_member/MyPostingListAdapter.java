package com.suno.bgm.ui.find_member;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.suno.bgm.R;
import com.suno.bgm.listener.OnItemClickListener;
import com.suno.bgm.model.find_member.FindMemberPosting;
import com.suno.bgm.model.find_member.CommentInfo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by suno on 2017. 8. 21..
 */

public class MyPostingListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
        implements OnItemClickListener {
    private Context context;
    private List<FindMemberPosting> itemList;

    public MyPostingListAdapter(Context context, List<FindMemberPosting> itemList) {
        this.context = context;
        this.itemList = itemList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        MyPostingListViewHolder vh = new MyPostingListViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_my_posting_list, parent, false), context);
        vh.setOnItemClickListener(this);

        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((MyPostingListViewHolder) holder).bindView(itemList.get(position));
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return itemList != null ? itemList.size() : 0 ;
    }

    @Override
    public void onItemClick(final int position) {
        final Intent intent = new Intent(context, ShowReqUserActivity.class);
        final List<CommentInfo> reqUserList = new ArrayList<>();

        DatabaseReference reqRef = FirebaseDatabase.getInstance().getReference();
        reqRef.child("comment").child(itemList.get(position).getKey()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot ds : dataSnapshot.getChildren()){
                    reqUserList.add(ds.getValue(CommentInfo.class));
                }

                intent.putExtra("reqUser", (Serializable) reqUserList);
                intent.putExtra("postKey", itemList.get(position).getKey());
                context.startActivity(intent);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onDeleteClick(int position) {

    }
}
