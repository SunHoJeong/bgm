package com.suno.bgm.ui.find_member;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.suno.bgm.R;
import com.suno.bgm.listener.OnItemClickListener;
import com.suno.bgm.model.find_member.FindMemberPosting;
import com.suno.bgm.util.SharedPreferencesService;

import java.util.List;

/**
 * Created by suno on 2017. 8. 13..
 */

public class PostingAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<FindMemberPosting> itemList;
    private SharedPreferencesService sharedPref;
    private OnItemClickListener listener;

    public PostingAdapter(Context context, List<FindMemberPosting> itemList, OnItemClickListener listener) {
        this.context = context;
        this.itemList = itemList;
        this.listener = listener;

        sharedPref = SharedPreferencesService.getInstance();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        PostingViewHold vh = new PostingViewHold(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_find_member_posting, parent, false), context);

        vh.setOnItemClickListener(listener);

        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((PostingViewHold) holder).bindView(itemList.get(position));
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return itemList != null ? itemList.size() : 0 ;
    }

}

