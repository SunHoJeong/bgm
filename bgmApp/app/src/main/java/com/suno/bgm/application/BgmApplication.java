package com.suno.bgm.application;

import android.app.Activity;
import android.app.Application;
import android.util.Log;

import com.facebook.stetho.Stetho;
import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.kakao.auth.KakaoSDK;
import com.suno.bgm.util.KakaoSDKAdapter;
import com.suno.bgm.util.SharedPreferencesService;
import com.suno.bgm.util.db.Database;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by suno on 2017. 8. 5..
 */

public class BgmApplication extends Application {
    public static final String BASE_URL = "http://13.124.165.223:3000/";
    public static final String YOUTUBE_URL = "https://www.googleapis.com/youtube/v3/";

    private static volatile BgmApplication mInstance = null;
    private static volatile Activity currentActivity = null;

    private static Retrofit retrofit = null;

    private static Database db;
    private SharedPreferencesService sharedPreferencesService;

    public static BgmApplication getGlobalApplicationContext() {
        if(mInstance == null)
            throw new IllegalStateException("this application does not inherit BgmApplication");
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;

        db = new Database(this);
        db.open();

        sharedPreferencesService = SharedPreferencesService.getInstance();
        sharedPreferencesService.load(this);

        KakaoSDK.init(new KakaoSDKAdapter());

        Stetho.initializeWithDefaults(this);
    }

    @Override
    public void onTerminate() {
        db.close();
        mInstance = null;

        super.onTerminate();
    }

    public static Activity getCurrentActivity() {
        Log.d("TAG", "++ currentActivity : " + (currentActivity != null ? currentActivity.getClass().getSimpleName() : ""));
        return currentActivity;
    }

    public static void setCurrentActivity(Activity currentActivity) {
        BgmApplication.currentActivity = currentActivity;
    }

    public static Retrofit getRetrofit(){
        if(retrofit == null){
            retrofit = new Retrofit.Builder()
                    .client(new OkHttpClient().newBuilder()
                            .addNetworkInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                            .addNetworkInterceptor(new StethoInterceptor()).build())
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        return retrofit;
    }
}