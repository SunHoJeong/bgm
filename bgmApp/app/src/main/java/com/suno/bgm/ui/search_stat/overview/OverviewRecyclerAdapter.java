package com.suno.bgm.ui.search_stat.overview;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.suno.bgm.R;
import com.suno.bgm.model.search_stat.UserStat;

/**
 * Created by suno on 2017. 8. 10..
 */

public class OverviewRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private UserStat userStat;


    public OverviewRecyclerAdapter(Context context, UserStat userStat) {
        this.context = context;
        this.userStat = userStat;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        OverviewViewHolder vh = new OverviewViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_match_history, parent, false), context);

        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((OverviewViewHolder) holder).bindView(userStat.getMatchHistory().get(position));
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return userStat.getMatchHistory() != null ? userStat.getMatchHistory().size() : 0;
    }
}
