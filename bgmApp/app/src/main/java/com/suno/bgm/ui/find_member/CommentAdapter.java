package com.suno.bgm.ui.find_member;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.suno.bgm.R;
import com.suno.bgm.model.find_member.CommentInfo;

import java.util.List;

/**
 * Created by suno on 2017. 8. 24..
 */

public class CommentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private List<CommentInfo> commentList;

    public CommentAdapter(List<CommentInfo> commentList, Context context) {
        this.commentList = commentList;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        CommentViewHolder vh = new CommentViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_comment, parent, false), context);

        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((CommentViewHolder) holder).bindView(commentList.get(position));
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return commentList != null ? commentList.size() : 0 ;
    }

    public void addDate(CommentInfo commentInfo){
        commentList.add(commentInfo);
        notifyItemChanged(commentList.size()-1);
    }

}
