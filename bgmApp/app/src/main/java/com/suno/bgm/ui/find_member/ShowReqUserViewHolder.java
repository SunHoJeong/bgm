package com.suno.bgm.ui.find_member;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.suno.bgm.R;
import com.suno.bgm.listener.OnItemClickListener;
import com.suno.bgm.model.find_member.CommentInfo;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by suno on 2017. 8. 21..
 */

public class ShowReqUserViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.textView_show_req_user_message)
    TextView tvMessage;
    @BindView(R.id.textView_show_req_user_id)
    TextView tvId;

    private Context context;
    private OnItemClickListener listener;

    public ShowReqUserViewHolder(View itemView, Context context) {
        super(itemView);
        this.context = context;

        ButterKnife.bind(this, itemView);
    }

    public void bindView(CommentInfo user){
        tvMessage.setText(user.getFcmBody().getBodyText());
        tvId.setText(user.getReqUser().getUserId());
    }

    public void setOnItemClickListener(OnItemClickListener listener){
        this.listener = listener;
    }

    @OnClick(R.id.button_show_req_user_accept)
    public void onAcceptClick(View v){
        listener.onItemClick(getAdapterPosition());
    }

    @OnClick(R.id.button_show_req_user_reject)
    public void onRejectClick(View v){
        listener.onDeleteClick(getAdapterPosition());
    }
}
