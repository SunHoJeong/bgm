package com.suno.bgm.model.search_stat;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;
import org.parceler.ParcelProperty;

/**
 * Created by suno on 2017. 8. 7..
 */
@Parcel(Parcel.Serialization.BEAN)
public class MatchHistory
{
    @SerializedName("Id")
    Integer id;
    @SerializedName("Updated")
    String updated;
    @SerializedName("UpdatedJS")
    String updatedJS;
    @SerializedName("Season")
    Integer season;
    @SerializedName("SeasonDisplay")
    String seasonDisplay;
    @SerializedName("Match")
    Integer match;
    @SerializedName("MatchDisplay")
    String matchDisplay;
    @SerializedName("Region")
    Integer region;
    @SerializedName("RegionDisplay")
    String regionDisplay;
    @SerializedName("Rounds")
    Integer rounds;
    @SerializedName("Wins")
    Integer wins;
    @SerializedName("Kills")
    Integer kills;
    @SerializedName("Assists")
    Integer assists;
    @SerializedName("Top10")
    Integer top10;
    @SerializedName("Rating")
    Double rating;
    @SerializedName("RatingChange")
    Double ratingChange;
    @SerializedName("RatingRank")
    Integer ratingRank;
    @SerializedName("RatingRankChange")
    Integer ratingRankChange;
    @SerializedName("Headshots")
    Integer headshots;
    @SerializedName("Kd")
    Double kd;
    @SerializedName("Damage")
    Integer damage;
    @SerializedName("TimeSurvived")
    Double timeSurvived;
    @SerializedName("MoveDistance")
    Double moveDistance;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public String getUpdatedJS() {
        return updatedJS;
    }

    public void setUpdatedJS(String updatedJS) {
        this.updatedJS = updatedJS;
    }

    public Integer getSeason() {
        return season;
    }

    public void setSeason(Integer season) {
        this.season = season;
    }

    public String getSeasonDisplay() {
        return seasonDisplay;
    }

    public void setSeasonDisplay(String seasonDisplay) {
        this.seasonDisplay = seasonDisplay;
    }

    public Integer getMatch() {
        return match;
    }

    public void setMatch(Integer match) {
        this.match = match;
    }

    public String getMatchDisplay() {
        return matchDisplay;
    }

    public void setMatchDisplay(String matchDisplay) {
        this.matchDisplay = matchDisplay;
    }

    public Integer getRegion() {
        return region;
    }

    public void setRegion(Integer region) {
        this.region = region;
    }

    public String getRegionDisplay() {
        return regionDisplay;
    }

    public void setRegionDisplay(String regionDisplay) {
        this.regionDisplay = regionDisplay;
    }

    public Integer getRounds() {
        return rounds;
    }

    public void setRounds(Integer rounds) {
        this.rounds = rounds;
    }

    public Integer getWins() {
        return wins;
    }

    public void setWins(Integer wins) {
        this.wins = wins;
    }

    public Integer getKills() {
        return kills;
    }

    public void setKills(Integer kills) {
        this.kills = kills;
    }

    public Integer getAssists() {
        return assists;
    }

    public void setAssists(Integer assists) {
        this.assists = assists;
    }

    public Integer getTop10() {
        return top10;
    }

    public void setTop10(Integer top10) {
        this.top10 = top10;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public Double getRatingChange() {
        return ratingChange;
    }

    public void setRatingChange(Double ratingChange) {
        this.ratingChange = ratingChange;
    }

    public Integer getRatingRank() {
        return ratingRank;
    }

    public void setRatingRank(Integer ratingRank) {
        this.ratingRank = ratingRank;
    }

    public Integer getRatingRankChange() {
        return ratingRankChange;
    }

    public void setRatingRankChange(Integer ratingRankChange) {
        this.ratingRankChange = ratingRankChange;
    }

    public Integer getHeadshots() {
        return headshots;
    }

    public void setHeadshots(Integer headshots) {
        this.headshots = headshots;
    }

    public Double getKd() {
        return kd;
    }

    public void setKd(Double kd) {
        this.kd = kd;
    }

    public Integer getDamage() {
        return damage;
    }

    public void setDamage(Integer damage) {
        this.damage = damage;
    }

    public Double getTimeSurvived() {
        return timeSurvived;
    }

    public void setTimeSurvived(Double timeSurvived) {
        this.timeSurvived = timeSurvived;
    }

    public Double getMoveDistance() {
        return moveDistance;
    }

    public void setMoveDistance(Double moveDistance) {
        this.moveDistance = moveDistance;
    }

}
