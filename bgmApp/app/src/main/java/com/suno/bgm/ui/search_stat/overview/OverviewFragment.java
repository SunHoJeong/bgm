package com.suno.bgm.ui.search_stat.overview;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.suno.bgm.R;
import com.suno.bgm.model.search_stat.UserStat;

import org.parceler.Parcels;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by suno on 2017. 8. 7..
 */

public class OverviewFragment extends Fragment {
    @BindView(R.id.recyclerView_match_history)
    RecyclerView recyclerView;

    private UserStat userStat;
    private OverviewRecyclerAdapter adapter;

    public static OverviewFragment newInstance(UserStat userStat){
        OverviewFragment fragment = new OverviewFragment();
        Bundle args = new Bundle();
        args.putParcelable(UserStat.class.getName(), Parcels.wrap(userStat));
        fragment.setArguments(args);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_search_stat_overview, container, false);
        ButterKnife.bind(this, v);

        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //userStat = savedInstanceState.getParcelable(UserStat.class.getName());

        Bundle args = getArguments();
        userStat = Parcels.unwrap(args.getParcelable(UserStat.class.getName()));

        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        adapter = new OverviewRecyclerAdapter(getActivity(), userStat);
        recyclerView.setAdapter(adapter);
    }

}
