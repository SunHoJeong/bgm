package com.suno.bgm.ui.find_member;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import com.suno.bgm.R;
import com.suno.bgm.model.find_member.CommentInfo;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by suno on 2017. 8. 20..
 */

public class ShowReqUserActivity extends AppCompatActivity {
    @BindView(R.id.recyclerView_show_req_user)
    RecyclerView rcvShowReqUser;
    @BindView(R.id.toolbar_show_req_user)
    Toolbar toolbar;
    @BindView(R.id.textView_show_req_user)
    TextView tvToolbarTitle;
    @BindView(R.id.textView_show_req_user_title)
    TextView tvTitle;

    private List<CommentInfo> itemList;
    private ShowReqUserAdapter adapter;
    private String postKey;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_req_user);
        ButterKnife.bind(this);

        getDataFromIntent();
        initToolbar();
        initRecyclerView();
    }

    public void initToolbar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tvToolbarTitle.setText(getResources().getString(R.string.show_req_user));
        tvTitle.setText(itemList.get(0).getFcmBody().getTitle());
    }

    public void getDataFromIntent(){
        Intent intent = getIntent();

        itemList = new ArrayList<>();
        itemList = (List<CommentInfo>) intent.getSerializableExtra("reqUser");
        postKey = intent.getStringExtra("postKey");
        Log.d("getData", itemList.size()+"");
    }

    public void initRecyclerView(){
        rcvShowReqUser.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rcvShowReqUser.setLayoutManager(layoutManager);

        adapter = new ShowReqUserAdapter(this, itemList, postKey);
        rcvShowReqUser.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
