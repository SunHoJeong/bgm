package com.suno.bgm;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.kakao.usermgmt.UserManagement;
import com.kakao.usermgmt.callback.LogoutResponseCallback;
import com.suno.bgm.model.find_member.FindMemberPosting;
import com.suno.bgm.ui.find_member.FindMemberFragment;
import com.suno.bgm.ui.find_member.MyPostingListActivity;
import com.suno.bgm.ui.info_board.InfoBoradFragment;
import com.suno.bgm.ui.login.LoginActivity;
import com.suno.bgm.ui.search_stat.SearchStatFragment;
import com.suno.bgm.ui.video_board.VideoBoradFragment;
import com.suno.bgm.util.SharedPreferencesService;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {
    public static final String FRAGMENT_FLAG = "fragmentFlag";
    public static final int FRAGMENT_SEARCH_STAT = 1;
    public static final int FRAGMENT_FIND_MEMBER = 2;

    private final long FINISH_INTERVAL_TIME = 2000;
    private long backPressedTime = 0;

    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottomNavigationView;
    @BindView(R.id.toolbar_main)
    Toolbar toolbar;
    @BindView(R.id.toolbar_title)
    TextView tvToolbarTitle;

    private SharedPreferencesService sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        init();
        sharedPref = SharedPreferencesService.getInstance();

        if(getIntent().getIntExtra(FRAGMENT_FLAG, -1) == -1){
            Log.d("MAIN", "null," +getIntent().getIntExtra(FRAGMENT_FLAG, -1)+","+ savedInstanceState);
            if(savedInstanceState == null){
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.add(R.id.main_container, new SearchStatFragment());
                ft.commit();
            }
        }
        else{
            Log.d("MAIN", "not null," +getIntent().getIntExtra(FRAGMENT_FLAG, -1)+","+ savedInstanceState);
            switch (getIntent().getIntExtra(FRAGMENT_FLAG, FRAGMENT_SEARCH_STAT)){
                case FRAGMENT_FIND_MEMBER:
                    bottomNavigationView.setSelectedItemId(R.id.action_two);
                    break;
            }
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void init(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Fragment fm = null;

                switch(item.getItemId()){
                    case R.id.action_one:
                        Log.d("SEL","selted one");
                        fm = new SearchStatFragment();
                        break;
                    case R.id.action_two:
                        if(sharedPref.getPrefBooleanData(SharedPreferencesService.KEY_LOGIN_STATE)){
                            //login state
                            fm = FindMemberFragment.getInstance();
                        }
                        else{
                            //TODO : 로그인 액티비티에서 백버튼 누르면 그냥 들어가짐
                            //logout state
                            redirectLoginActivity();
                            fm = FindMemberFragment.getInstance();
                        }
                        break;
                    case R.id.action_three:
                        fm = new InfoBoradFragment();
                        break;
                    case R.id.action_four:
                        fm = new VideoBoradFragment();
                        break;
                }

                if(fm != null){
                    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.main_container, fm);
                    ft.commit();
                }

                return true;
            }
        });

    }

    public void setToolbarTitle(String title){
        tvToolbarTitle.setText(title);
    }

    protected void redirectLoginActivity() {
        final Intent intent = new Intent(this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_setting, menu);

        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean isLogin = sharedPref.getPrefBooleanData(SharedPreferencesService.KEY_LOGIN_STATE, false);

        if(isLogin){
            menu.getItem(0).setEnabled(false);
            menu.getItem(1).setEnabled(true);
            menu.getItem(2).setEnabled(true);
        }
        else{
            menu.getItem(0).setEnabled(true);
            menu.getItem(1).setEnabled(false);
            menu.getItem(2).setEnabled(false);
        }

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int selectedItem = item.getItemId();

        switch (selectedItem){
            case R.id.menu_login :
                Intent intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                return true;
            case R.id.menu_logout :
                Toast.makeText(this, "로그아웃", Toast.LENGTH_SHORT).show();

                UserManagement.requestLogout(new LogoutResponseCallback() {
                    @Override
                    public void onCompleteLogout() {
                        sharedPref.setPrefData(SharedPreferencesService.KEY_LOGIN_STATE, false);
                        redirectLoginActivity();
                    }
                });
                return true;
            case R.id.menu_show_my_posting :
                redirectMyPostingListActivity();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        long tempTime = System.currentTimeMillis();
        long intervalTime = tempTime - backPressedTime;

        if(0 <= intervalTime && intervalTime <= FINISH_INTERVAL_TIME){
            super.onBackPressed();
        }
        else {
            backPressedTime = tempTime;
            Toast.makeText(this, "뒤로가기 버튼을 한번 더 누르시면 종료됩니다", Toast.LENGTH_SHORT).show();
        }
    }

    public void redirectMyPostingListActivity(){
        final Intent intent = new Intent(this, MyPostingListActivity.class);
        final List<FindMemberPosting> myPostingList = new ArrayList<>();

        DatabaseReference postingRef = FirebaseDatabase.getInstance().getReference();
        postingRef.child("user-posts").child(sharedPref.getPrefStringData(SharedPreferencesService.KEY_USER_EMAIL)
                .split("\\.")[0]).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot ds : dataSnapshot.getChildren()){
                    myPostingList.add(ds.getValue(FindMemberPosting.class));
                }

                intent.putExtra("myPosting", (Serializable) myPostingList);
                intent.putExtra("myId", sharedPref.getPrefStringData(SharedPreferencesService.KEY_USER_EMAIL));
                startActivity(intent);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
