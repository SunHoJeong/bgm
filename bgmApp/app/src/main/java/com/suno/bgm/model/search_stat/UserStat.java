package com.suno.bgm.model.search_stat;


import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.List;

/**
 * Created by suno on 2017. 8. 9..
 */
@Parcel(Parcel.Serialization.BEAN)
public class UserStat
{
    Integer platformId;
    @SerializedName("AccountId")
    String accountId;
    @SerializedName("Avatar")
    String avatar;
    String selectedRegion;
    String defaultSeason;
    String seasonDisplay;
    @SerializedName("LastUpdated")
    String lastUpdated;
    @SerializedName("LiveTracking")
    List<LiveTracking> liveTracking = null;
    @SerializedName("PlayerName")
    String playerName;
    @SerializedName("PubgTrackerId")
    Integer pubgTrackerId;
    @SerializedName("Stats")
    List<Stat> stats = null;
    @SerializedName("MatchHistory")
    List<MatchHistory> matchHistory = null;

    public Integer getPlatformId() {
        return platformId;
    }

    public void setPlatformId(Integer platformId) {
        this.platformId = platformId;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getSelectedRegion() {
        return selectedRegion;
    }

    public void setSelectedRegion(String selectedRegion) {
        this.selectedRegion = selectedRegion;
    }

    public String getDefaultSeason() {
        return defaultSeason;
    }

    public void setDefaultSeason(String defaultSeason) {
        this.defaultSeason = defaultSeason;
    }

    public String getSeasonDisplay() {
        return seasonDisplay;
    }

    public void setSeasonDisplay(String seasonDisplay) {
        this.seasonDisplay = seasonDisplay;
    }

    public String getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(String lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public List<LiveTracking> getLiveTracking() {
        return liveTracking;
    }

    public void setLiveTracking(List<LiveTracking> liveTracking) {
        this.liveTracking = liveTracking;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public Integer getPubgTrackerId() {
        return pubgTrackerId;
    }

    public void setPubgTrackerId(Integer pubgTrackerId) {
        this.pubgTrackerId = pubgTrackerId;
    }

    public List<Stat> getStats() {
        return stats;
    }

    public void setStats(List<Stat> stats) {
        this.stats = stats;
    }

    public List<MatchHistory> getMatchHistory() {
        return matchHistory;
    }

    public void setMatchHistory(List<MatchHistory> matchHistory) {
        this.matchHistory = matchHistory;
    }

}
