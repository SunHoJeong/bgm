package com.suno.bgm.ui.find_member;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.suno.bgm.R;
import com.suno.bgm.application.BgmApplication;
import com.suno.bgm.listener.OnItemClickListener;
import com.suno.bgm.model.find_member.CommentInfo;
import com.suno.bgm.util.network.HttpService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.media.CamcorderProfile.get;

/**
 * Created by suno on 2017. 8. 21..
 */

public class ShowReqUserAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
                                implements OnItemClickListener{
    private Context context;
    private List<CommentInfo> itemList;
    private String postKey;

    public ShowReqUserAdapter(Context context, List<CommentInfo> itemList, String postKey) {
        this.context = context;
        this.itemList = itemList;
        this.postKey = postKey;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ShowReqUserViewHolder vh = new ShowReqUserViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_show_req_user, parent, false), context);
        vh.setOnItemClickListener(this);

        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((ShowReqUserViewHolder) holder).bindView(itemList.get(position));
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return itemList != null ? itemList.size() : 0 ;
    }

    @Override
    public void onItemClick(final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("같이하기 신청");
        builder.setMessage("신청을 수락 하시겠습니까?");
        builder.setPositiveButton("예",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        pushFcm(position);

                    }
                });
        builder.setNegativeButton("아니오",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
        builder.show();
    }

    @Override
    public void onDeleteClick(final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("같이하기 신청");
        builder.setMessage("신청을 거절 하시겠습니까?");
        builder.setPositiveButton("예",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        CommentInfo commentInfo = itemList.get(position);
                        final String key = commentInfo.getKey();
                        Log.d("DELETE", key+",,"+postKey+",,"+commentInfo.getPostKey());
                        DatabaseReference commentRef = FirebaseDatabase.getInstance().getReference();
                        commentRef.child("comment").child(postKey).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                for(DataSnapshot snapshot : dataSnapshot.getChildren()){
                                    if(snapshot.getKey().equals(key)){
                                        snapshot.getRef().removeValue();
                                    }
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });
                        itemList.remove(position);
                        notifyItemRemoved(position);
                        //TODO: database에서 삭제
                        //DatabaseReference commentRef = FirebaseDatabase.getInstance().getReference();

                    }
                });
        builder.setNegativeButton("아니오",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
        builder.show();
    }

    public void pushFcm(int position){
        HttpService service = BgmApplication.getRetrofit().create(HttpService.class);
        //service.
        Call<String> call = service.postFcmPushToUser(itemList.get(position));
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Log.d("FCM", "보내기 성공");
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }
}
