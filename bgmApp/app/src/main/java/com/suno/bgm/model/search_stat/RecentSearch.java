package com.suno.bgm.model.search_stat;

import com.suno.bgm.base.BaseItem;

import java.util.ArrayList;

/**
 * Created by suno on 2017. 8. 8..
 */

public class RecentSearch extends BaseItem {
    private String searchId;

    public RecentSearch(int viewType) {
        super(viewType);
    }

    public String getSearchId() {
        return searchId;
    }

    public void setSearchId(String searchId) {
        this.searchId = searchId;
    }
}
