package com.suno.bgm.ui.find_member;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.suno.bgm.R;
import com.suno.bgm.model.find_member.FindMemberPosting;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by suno on 2017. 8. 24..
 */

public class PostingTopFragment extends Fragment {
    @BindView(R.id.textView_positng_top_title)
    TextView tvTitle;
    @BindView(R.id.textView_positng_top_author)
    TextView tvAuthor;
    @BindView(R.id.textView_positng_top_date)
    TextView tvDate;
    @BindView(R.id.textView_positng_top_body_text)
    TextView tvBodyText;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_posting_top, container, false);
        ButterKnife.bind(this, v);

//        ((MainActivity)getActivity()).setToolbarTitle(getResources().getString(R.string.find_member));

        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public void updateView(FindMemberPosting posting){
        //작성자, 글제목, 글내용, 시간
        tvTitle.setText(posting.getTitle());
        tvAuthor.setText(posting.getAuthor());
        tvDate.setText(posting.getDate());
        tvBodyText.setText(posting.getBodyText());
    }
}
