package com.suno.bgm.ui.video_board;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.suno.bgm.MainActivity;
import com.suno.bgm.R;
import com.suno.bgm.model.youtube.SearchData;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by suno on 2017. 8. 18..
 */

public class VideoDetailFragment extends Fragment {
    @BindView(R.id.textView_video_detail_title)
    TextView tvTitle;
    @BindView(R.id.textView_video_detail_description)
    TextView tvDescription;
    @BindView(R.id.textView_video_detail_channel_title)
    TextView tvChannelTitle;

    private View v;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_video_detail, container, false);
        ButterKnife.bind(this, v);

        ((MainActivity)getActivity()).setToolbarTitle(getResources().getString(R.string.video_board));

        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public void updateView(SearchData searchData){
        tvTitle.setText(searchData.getSnippet().getTitle());
        tvChannelTitle.setText(searchData.getSnippet().getChannelTitle());
        tvDescription.setText(searchData.getSnippet().getDescription());
        //Glide.with(this).load(searchData.getSnippet().);
    }
}
