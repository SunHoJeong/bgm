package com.suno.bgm.ui.video_board;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.pedrovgs.DraggablePanel;
import com.google.android.youtube.player.YouTubePlayer;
import com.suno.bgm.R;
import com.suno.bgm.listener.OnItemClickListener;
import com.suno.bgm.model.youtube.SearchData;

import java.util.List;

/**
 * Created by suno on 2017. 8. 20..
 */

public class VideoRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private List<SearchData> itemList;
    private OnItemClickListener listener;

    public VideoRecyclerAdapter(Context context, List<SearchData> itemList, OnItemClickListener listener ) {
        this.context = context;
        this.itemList = itemList;
        this.listener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        VideoViewHolder vh = new VideoViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_video, parent, false), context );

        vh.setOnItemClickListener(listener);

        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Log.d("Bind", position+"");
        ((VideoViewHolder)holder).bindView(itemList.get(position));
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return itemList != null ? itemList.size() : 0 ;
    }

    public void setItemList(List<SearchData> itemList){
        this.itemList = itemList;
        notifyDataSetChanged();
    }



}
