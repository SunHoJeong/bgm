package com.suno.bgm.ui.video_board;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.suno.bgm.R;
import com.suno.bgm.listener.OnItemClickListener;
import com.suno.bgm.model.youtube.SearchData;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by suno on 2017. 8. 20..
 */

public class VideoViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.imageView_video_thumbnail)
    ImageView imgvThumbnail;
    @BindView(R.id.textView_video_title)
    TextView tvTitle;
    @BindView(R.id.textView_video_channel)
    TextView tvChannel;
    @BindView(R.id.textView_video_date)
    TextView tvDate;

    private Context context;
    private OnItemClickListener listener;
    private SearchData searchData;

    public VideoViewHolder(View itemView, Context context) {
        super(itemView);

        this.context = context;

        ButterKnife.bind(this, itemView);
    }

    public void bindView(SearchData item) {
        searchData = item;

        Glide.with(context).load(searchData.getSnippet().getThumbnails().getMedium().getUrl()).into(imgvThumbnail);
        tvTitle.setText(searchData.getSnippet().getTitle());
        tvChannel.setText(searchData.getSnippet().getChannelTitle());

    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @OnClick(R.id.linearLayout_video_item)
    public void onItemClick(View v) {
        listener.onItemClick(getAdapterPosition());
    }


}
