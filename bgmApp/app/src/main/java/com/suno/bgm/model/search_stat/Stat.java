package com.suno.bgm.model.search_stat;


import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.List;

/**
 * Created by suno on 2017. 8. 7..
 */
@Parcel(Parcel.Serialization.BEAN)
public class Stat
{
    @SerializedName("Region")
    String region;
    @SerializedName("Season")
    String season;
    @SerializedName("Match")
    String match;
    @SerializedName("Stats")
    List<Stat_> stats = null;

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    public String getMatch() {
        return match;
    }

    public void setMatch(String match) {
        this.match = match;
    }

    public List<Stat_> getStats() {
        return stats;
    }

    public void setStats(List<Stat_> stats) {
        this.stats = stats;
    }

}

