package com.suno.bgm.model.find_member;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by suno on 2017. 8. 15..
 */

public class BgmUser implements Serializable{
    private String userId;
    private String userName;
    private String fcmToken;
    private String userProfileUrl;

    public void User(){};

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }

    public String getUserProfileUrl() {
        return userProfileUrl;
    }

    public void setUserProfileUrl(String userProfileUrl) {
        this.userProfileUrl = userProfileUrl;
    }

    public Map<String, Object> toMap(){
        HashMap<String, Object> result = new HashMap<>();
        result.put("userId", userId);
        result.put("userName", userName);
        result.put("fcmToken", fcmToken);
        result.put("userProfileUrl", userProfileUrl);

        return result;
    }
}
