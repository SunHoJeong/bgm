package com.suno.bgm.ui.search_stat;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ybq.android.spinkit.SpinKitView;
import com.suno.bgm.R;
import com.suno.bgm.application.BgmApplication;
import com.suno.bgm.model.search_stat.RecentSearch;
import com.suno.bgm.model.search_stat.UserStat;
import com.suno.bgm.util.db.Database;
import com.suno.bgm.util.network.HttpService;

import org.parceler.Parcels;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.suno.bgm.base.BaseItem.TYPE_RECENT_SEARCH;

/**
 * Created by suno on 2017. 8. 7..
 */

public class SearchStatActivity extends AppCompatActivity implements Callback<UserStat> {
    @BindView(R.id.toolbar_search_stat)
    Toolbar toolbarSearchStat;
    @BindView(R.id.editText_search_stat)
    EditText etSearchId;
    @BindView(R.id.recyclerView_recent_search)
    RecyclerView recyclerView;
    @BindView(R.id.spinKitView_search_stat)
    SpinKitView spinKitView;

    @BindView(R.id.frameLayout_background)
    FrameLayout frameLayout;

    private RecentSearchRecyclerAdapter adapter;
    private List<RecentSearch> itemList;

    private UserStat userStat = null;
    private RecentSearch recentSearch;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_stat);
        ButterKnife.bind(this);

        frameLayout.setVisibility(View.INVISIBLE);

        initToolbar();
        initRecyclerView();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //TODO: 최근전적 검색 아이디 중복 제거
    public void saveRecentSearch(String searchId) {
        recentSearch = new RecentSearch(TYPE_RECENT_SEARCH);
        recentSearch.setSearchId(searchId);

        if (Database.mRecentSearchDao.addData(recentSearch)) {
            itemList.add(recentSearch);
            adapter.notifyItemInserted(itemList.size() - 1);

            Log.d("RETRO", "33");
            Log.d("SAVE", recentSearch.getSearchId() + " , " + itemList.size());
            Log.d("RecentSearchTask", "최근검색 저장성공");
        } else {
            Log.d("RecentSearchTask", "최근검색 저장실패");
        }
    }

    public void redirectSearchStatResultActivity(UserStat userStat) {
        Log.d("RETRO", "44");
        Intent intent = new Intent(this, SearchStatResultActivity.class);
        intent.putExtra(UserStat.class.getName(), Parcels.wrap(userStat));
        startActivity(intent);
    }

    public void initToolbar() {
        setSupportActionBar(toolbarSearchStat);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        etSearchId.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                switch (actionId) {
                    case EditorInfo.IME_ACTION_SEARCH:
                        Toast.makeText(getApplicationContext(), etSearchId.getText().toString(), Toast.LENGTH_SHORT).show();
                        String searchId = etSearchId.getText().toString();
                        saveRecentSearch(searchId);

                        HttpService service = BgmApplication.getRetrofit().create(HttpService.class);
                        Call<UserStat> call = service.getUserStat(searchId);

                        frameLayout.setVisibility(View.VISIBLE);

                        call.enqueue(SearchStatActivity.this);

                        return true;

                    default:
                        Toast.makeText(getApplicationContext(), "기본", Toast.LENGTH_LONG).show();
                        return false;
                }
            }
        });
    }

    public void initRecyclerView() {
        itemList = Database.mRecentSearchDao.fetchAllDatas();

        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        adapter = new RecentSearchRecyclerAdapter(this, itemList);
        recyclerView.setAdapter(adapter);
    }


    /* Retrofit Callback */
    @Override
    public void onResponse(Call<UserStat> call, Response<UserStat> response) {
        frameLayout.setVisibility(View.INVISIBLE);
        etSearchId.setText("");

        if (response.code() == 200) {
            userStat = response.body();

            if (userStat.getAccountId() != null) {
                redirectSearchStatResultActivity(userStat);
            } else {
                Toast.makeText(SearchStatActivity.this, "아이디가 존재하지 않습니다!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(SearchStatActivity.this, "다시 시도해주세요!", Toast.LENGTH_SHORT).show();

        }

    }

    @Override
    public void onFailure(Call<UserStat> call, Throwable t) {
        frameLayout.setVisibility(View.INVISIBLE);
        Toast.makeText(SearchStatActivity.this, t.getMessage().toString(), Toast.LENGTH_SHORT).show();
    }

}
