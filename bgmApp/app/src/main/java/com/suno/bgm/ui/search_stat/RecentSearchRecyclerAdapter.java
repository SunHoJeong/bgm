package com.suno.bgm.ui.search_stat;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.suno.bgm.R;
import com.suno.bgm.application.BgmApplication;
import com.suno.bgm.base.BaseItem;
import com.suno.bgm.listener.OnItemClickListener;
import com.suno.bgm.model.search_stat.RecentSearch;
import com.suno.bgm.model.search_stat.UserStat;
import com.suno.bgm.util.db.Database;
import com.suno.bgm.util.network.HttpService;

import java.util.List;

import retrofit2.Call;

/**
 * Created by suno on 2017. 8. 8..
 */

public class RecentSearchRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
        implements OnItemClickListener {
    Context context;
    List<RecentSearch> itemList;

    public RecentSearchRecyclerAdapter(Context context, List<RecentSearch> itemList) {
        this.context = context;
        this.itemList = itemList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case BaseItem.TYPE_RECENT_SEARCH:
                RecentSearchViewHolder vh = new RecentSearchViewHolder(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_recent_search, parent, false), context);

                vh.setOnItemClickListener(this);

                return vh;
        }
        return null;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case BaseItem.TYPE_RECENT_SEARCH:
                ((RecentSearchViewHolder) holder).bindView(itemList.get(position));
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return itemList.get(position).getViewType();
    }

    @Override
    public int getItemCount() {
        return itemList != null ? itemList.size() : 0;
    }

    @Override
    public void onDeleteClick(int position) {
        Database.mRecentSearchDao.deleteData(itemList.get(position).getSearchId());
        itemList.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public void onItemClick(int position) {
        String itemId = itemList.get(position).getSearchId();

        HttpService service = BgmApplication.getRetrofit().create(HttpService.class);
        Call<UserStat> call = service.getUserStat(itemId);
        ((SearchStatActivity) context).frameLayout.setVisibility(View.VISIBLE);

        call.enqueue(((SearchStatActivity) context));

        ((SearchStatActivity)context).saveRecentSearch(itemId);
    }

}
