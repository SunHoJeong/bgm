package com.suno.bgm.model;

import android.content.Context;

import java.util.ArrayList;

/**
 * Created by suno on 2017. 8. 3..
 */

public interface SampleImageSource {
    interface LoadImageCallback {
        void onImageLoaded(ArrayList<ImageItem> list);
    }

    void getImages(Context context, int size, LoadImageCallback loadImageCallback);
}
