package com.suno.bgm.util.network;

import com.suno.bgm.model.find_member.FcmBody;
import com.suno.bgm.model.find_member.CommentInfo;
import com.suno.bgm.model.search_stat.UserStat;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by suno on 2017. 8. 9..
 */

public interface HttpService {
    @GET("search/{searchId}")
    Call<UserStat> getUserStat(@Path("searchId") String searchId);

    @POST("fcm/writer")
    Call<String> postFcmPushToWriter(@Body FcmBody fcmBody);

    @POST("fcm/user")
    Call<String> postFcmPushToUser(@Body CommentInfo commentInfo);

}
