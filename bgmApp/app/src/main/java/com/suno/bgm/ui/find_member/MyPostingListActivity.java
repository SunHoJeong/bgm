package com.suno.bgm.ui.find_member;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.suno.bgm.R;
import com.suno.bgm.model.find_member.FindMemberPosting;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by suno on 2017. 8. 21..
 */

public class MyPostingListActivity extends AppCompatActivity {
    @BindView(R.id.recyclerView_my_posting_list)
    RecyclerView rcvPostList;
    @BindView(R.id.toolbar_my_post_toolbar)
    Toolbar toolbar;
    @BindView(R.id.textView_my_post_toolbar_title)
    TextView tvToolbarTitle;
    @BindView(R.id.textView_my_post_id)
    TextView tvId;

    private List<FindMemberPosting> itemList;
    private String myId;
    private MyPostingListAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_posting_list);
        ButterKnife.bind(this);

        getDataFromIntent();
        initToolbar();
        initRecyclerView();
    }

    public void initToolbar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tvToolbarTitle.setText(getResources().getString(R.string.my_post_list));
    }

    public void getDataFromIntent(){
        Intent intent = getIntent();

        itemList = new ArrayList<>();
        itemList = (List<FindMemberPosting>) intent.getSerializableExtra("myPosting");
        myId = intent.getStringExtra("myId");

        tvId.setText(myId);

        Log.d("getData", itemList.size()+"");
    }

    public void initRecyclerView(){
        rcvPostList.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rcvPostList.setLayoutManager(layoutManager);

        adapter = new MyPostingListAdapter(this, itemList);
        rcvPostList.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}